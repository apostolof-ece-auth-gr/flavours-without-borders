from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings

class Role(models.Model):
    role_id = models.IntegerField(primary_key=True)
    role_name = models.CharField(max_length=500)
    role_description = models.CharField(max_length=700)

    class Meta:
        managed = True
        db_table = 'role'

class User(AbstractUser):
    role = models.ForeignKey(Role, models.DO_NOTHING, default=0)
    user_age = models.DateField(null=True)

    class Meta:
        managed = True
        db_table = 'user'

class Diet(models.Model):
    diet_id = models.AutoField(primary_key=True)
    diet_name = models.CharField(max_length=500)
    diet_description = models.CharField(max_length=700, blank=True, null=True)
    diet_is_approved = models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = 'diet'

class DietProhibitsIngredient(models.Model):
    diet = models.ForeignKey(Diet, models.DO_NOTHING)
    ingredient_name = models.ForeignKey('Ingredient', models.DO_NOTHING, db_column='ingredient_name')

    class Meta:
        managed = True
        db_table = 'diet_prohibits_ingredient'
        unique_together = (('diet', 'ingredient_name'),)

class Drink(models.Model):
    drink_id = models.AutoField(primary_key=True)
    drink_name = models.CharField(max_length=500)
    drink_description = models.CharField(max_length=700, blank=True, null=True)
    drink_is_approved = models.BooleanField(default=False)
    restaurant = models.ForeignKey('Restaurant', models.DO_NOTHING)

    class Meta:
        managed = True
        db_table = 'drink'

class DrinkHasIngredient(models.Model):
    drink = models.ForeignKey(Drink, models.DO_NOTHING)
    ingredient_name = models.ForeignKey('Ingredient', models.DO_NOTHING, db_column='ingredient_name')

    class Meta:
        managed = True
        db_table = 'drink_has_ingredient'
        unique_together = (('drink', 'ingredient_name'),)

class Food(models.Model):
    food_id = models.AutoField(primary_key=True)
    food_name = models.CharField(max_length=500)
    food_description = models.CharField(max_length=700, blank=True, null=True)
    food_calories = models.IntegerField(blank=True, null=True)
    food_is_approved = models.BooleanField(default=False)
    restaurant = models.ForeignKey('Restaurant', models.DO_NOTHING)

    class Meta:
        managed = True
        db_table = 'food'

class FoodHasIngredient(models.Model):
    food = models.ForeignKey(Food, models.DO_NOTHING)
    ingredient_name = models.ForeignKey('Ingredient', models.DO_NOTHING, db_column='ingredient_name')

    class Meta:
        managed = True
        db_table = 'food_has_ingredient'
        unique_together = (('food', 'ingredient_name'),)

class Ingredient(models.Model):
    ingredient_name = models.CharField(primary_key=True, max_length=500)
    ingredient_has_alcohol = models.BooleanField(default=False, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'ingredient'

class Permission(models.Model):
    permission_id = models.AutoField(primary_key=True)
    permission_description = models.CharField(max_length=700)

    class Meta:
        managed = True
        db_table = 'permission'

class Restaurant(models.Model):
    restaurant_id = models.AutoField(primary_key=True)
    restaurant_name = models.CharField(max_length=500)
    restaurant_category = models.CharField(max_length=10)
    restaurant_longitude = models.FloatField()
    restaurant_latitude = models.FloatField()
    restaurant_opening = models.TimeField(blank=True, null=True)
    restaurant_closing = models.TimeField(blank=True, null=True)
    restaurant_is_approved = models.BooleanField(default=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'restaurant'

class RoleHasPermission(models.Model):
    role = models.ForeignKey(Role, models.DO_NOTHING)
    permission = models.ForeignKey(Permission, models.DO_NOTHING)

    class Meta:
        managed = True
        db_table = 'role_has_permission'
        unique_together = (('role', 'permission'),)

class UserFollowsDiet(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.DO_NOTHING)
    diet = models.ForeignKey(Diet, models.DO_NOTHING)

    class Meta:
        managed = True
        db_table = 'user_follows_diet'
        unique_together = (('user', 'diet'),)

class UserProhibitsIngredient(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.DO_NOTHING)
    ingredient_name = models.ForeignKey(Ingredient, models.DO_NOTHING, db_column='ingredient_name')

    class Meta:
        managed = True
        db_table = 'user_prohibits_ingredient'
        unique_together = (('user', 'ingredient_name'),)

class UserRatesDrink(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.DO_NOTHING, blank=True, null=True)
    drink = models.ForeignKey(Drink, models.DO_NOTHING)
    rating_grade = models.IntegerField()
    rating_date = models.DateField(auto_now=True)
    rating_text = models.CharField(max_length=700, blank=True, null=True)
    rating_pοrtion_size = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'user_rates_drink'
        unique_together = (('user', 'drink'),)

class UserRatesFood(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.DO_NOTHING, blank=True, null=True)
    food = models.ForeignKey(Food, models.DO_NOTHING)
    rating_grade = models.IntegerField()
    rating_date = models.DateField(auto_now=True)
    rating_text = models.CharField(max_length=700, blank=True, null=True)
    rating_pοrtion_size = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'user_rates_food'
        unique_together = (('user', 'food'),)

class UserRatesRestaurant(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.DO_NOTHING, blank=True, null=True)
    restaurant = models.ForeignKey(Restaurant, models.DO_NOTHING)
    diet = models.ForeignKey(Diet, models.DO_NOTHING, blank=True, null=True)
    rating_grade = models.IntegerField()
    rating_date = models.DateField(auto_now=True)
    rating_text = models.CharField(max_length=700, blank=True, null=True)
    rating_accessibility = models.CharField(max_length=8, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'user_rates_restaurant'
        unique_together = (('user', 'restaurant'),)
