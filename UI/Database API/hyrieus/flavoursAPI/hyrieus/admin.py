from django.contrib import admin
from .models import (Diet,
	DietProhibitsIngredient, Drink, DrinkHasIngredient, Food, FoodHasIngredient,
	Ingredient, Permission, Restaurant, Role, RoleHasPermission, User,
	UserFollowsDiet, UserProhibitsIngredient, UserRatesDrink, UserRatesFood,
	UserRatesRestaurant,
)

admin.site.register(Diet)
admin.site.register(DietProhibitsIngredient)
admin.site.register(Drink)
admin.site.register(DrinkHasIngredient)
admin.site.register(Food)
admin.site.register(FoodHasIngredient)
admin.site.register(Ingredient)
admin.site.register(Permission)
admin.site.register(Restaurant)
admin.site.register(Role)
admin.site.register(RoleHasPermission)
admin.site.register(User)
admin.site.register(UserFollowsDiet)
admin.site.register(UserProhibitsIngredient)
admin.site.register(UserRatesDrink)
admin.site.register(UserRatesFood)
admin.site.register(UserRatesRestaurant)