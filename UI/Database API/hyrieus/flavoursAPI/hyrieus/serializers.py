from rest_framework import serializers
from rest_framework.authtoken.models import Token
from .models import (
	Diet, DietProhibitsIngredient, Drink, DrinkHasIngredient, Food,
	FoodHasIngredient, Ingredient, Permission, Restaurant, Role,
	RoleHasPermission, User, UserFollowsDiet, UserProhibitsIngredient,
	UserRatesDrink, UserRatesFood, UserRatesRestaurant,
)

class DietSerializer(serializers.ModelSerializer):
	class Meta:
		model = Diet
		fields = '__all__'

class DietProhibitsIngredientSerializer(serializers.ModelSerializer):
	class Meta:
		model = DietProhibitsIngredient
		fields = '__all__'

class DrinkSerializer(serializers.ModelSerializer):
	class Meta:
		model = Drink
		fields = '__all__'

class DrinkHasIngredientSerializer(serializers.ModelSerializer):
	class Meta:
		model = DrinkHasIngredient
		fields = '__all__'

class FoodSerializer(serializers.ModelSerializer):
	class Meta:
		model = Food
		fields = '__all__'

class FoodHasIngredientSerializer(serializers.ModelSerializer):
	class Meta:
		model = FoodHasIngredient
		fields = '__all__'

class IngredientSerializer(serializers.ModelSerializer):
	class Meta:
		model = Ingredient
		fields = '__all__'

class PermissionSerializer(serializers.ModelSerializer):
	class Meta:
		model = Permission
		fields = '__all__'

class RestaurantSerializer(serializers.ModelSerializer):
	class Meta:
		model = Restaurant
		fields = '__all__'

	def create(self, validated_data):
		user = None
		request = self.context.get("request")
		if request and hasattr(request, "user"):
			user = request.user
			user = User.objects.get(username=user)
			validated_data['restaurant_is_approved'] = True if (user.role.role_id == 1) else False
		restaurant = Restaurant.objects.create(**validated_data)
		return restaurant

class RoleSerializer(serializers.ModelSerializer):
	class Meta:
		model = Role
		fields = '__all__'

class RoleHasPermissionSerializer(serializers.ModelSerializer):
	class Meta:
		model = RoleHasPermission
		fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = '__all__'

class UserPublicInfoSerializer(UserSerializer):
	class Meta:
		model = User
		fields = (
			'id',
			'role',
			'username'
		)

class UserFollowsDietSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserFollowsDiet
		fields = '__all__'

class UserProhibitsIngredientSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserProhibitsIngredient
		fields = '__all__'

class UserRatesDrinkSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserRatesDrink
		fields = '__all__'

class UserRatesFoodSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserRatesFood
		fields = '__all__'

class UserRatesRestaurantSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserRatesRestaurant
		fields = '__all__'

class FlavoursTokenSerializer(serializers.ModelSerializer):
	user = UserPublicInfoSerializer(many=False, read_only=True)
	class Meta:
		model = Token
		fields = ('key', 'user')