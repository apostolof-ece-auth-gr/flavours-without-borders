from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers
from flavoursAPI.hyrieus.views import (DietViewSet, DietProhibitsIngredientViewSet,
    DrinkViewSet, DrinkHasIngredientViewSet, FoodViewSet, FoodHasIngredientViewSet,
    IngredientViewSet, PermissionViewSet, RestaurantViewSet, RoleViewSet,
    RoleHasPermissionViewSet, UserViewSet, UserFollowsDietViewSet,
    UserProhibitsIngredientViewSet, UserRatesDrinkViewSet, UserRatesFoodViewSet,
    UserRatesRestaurantViewSet, UserSetBirthDay, RestaurantUserView, UserDiets,
    FoodUserView, DrinkUserView, ProfileUserView, AddFood, AddDrink, AddDiet,
    FollowDiet, ProhibitIngredient, DietUserView, IngredientUserView,
    AddIngredientToFood, AddIngredientToDrink, AcceptRestaurant,AcceptFood,
    AcceptDrink, AcceptDiet,
)

router = routers.DefaultRouter()
router.register(r'diet', DietViewSet)
router.register(r'dietprohibitsingredient', DietProhibitsIngredientViewSet)
router.register(r'drink', DrinkViewSet)
router.register(r'drinkhasingredient', DrinkHasIngredientViewSet)
router.register(r'food', FoodViewSet)
router.register(r'foodhasingredient', FoodHasIngredientViewSet)
router.register(r'ingredient', IngredientViewSet)
router.register(r'permission', PermissionViewSet)
router.register(r'restaurant', RestaurantViewSet)
router.register(r'role', RoleViewSet)
router.register(r'rolehaspermission', RoleHasPermissionViewSet)
router.register(r'user', UserViewSet)
router.register(r'userfollowsdiet', UserFollowsDietViewSet)
router.register(r'userprohibitsingredient', UserProhibitsIngredientViewSet)
router.register(r'userratesdrink', UserRatesDrinkViewSet)
router.register(r'userratesfood', UserRatesFoodViewSet)
router.register(r'userratesrestaurant', UserRatesRestaurantViewSet)

urlpatterns = [
    path('setUserBirthday/<int:pk>/', UserSetBirthDay.as_view(), name='setUserBirthday'),
    path('restaurantUserView/<int:restaurant>/', RestaurantUserView.as_view(), name='restaurantUserView'),
    path('foodUserView/<int:food>/', FoodUserView.as_view(), name='foodUserView'),
    path('drinkUserView/<int:drink>/', DrinkUserView.as_view(), name='drinkUserView'),
    path('profileUserView/<int:profile>/', ProfileUserView.as_view(), name='profileUserView'),
    path('userDiets/', UserDiets.as_view(), name='userDiets'),
    path('addFood/', AddFood.as_view(), name='addFood'),
    path('addDrink/', AddDrink.as_view(), name='addDrink'),
    path('addDiet/', AddDiet.as_view(), name='addDiet'),
    path('followDiet/<int:diet>/', FollowDiet.as_view(), name='followDiet'),
    path('prohibitIngredient/<ingredient>/', ProhibitIngredient.as_view(), name='prohibitIngredient'),
    path('dietUserView/', DietUserView.as_view(), name='dietUserView'),
    path('ingredientUserView/', IngredientUserView.as_view(), name='ingredientUserView'),
    path('addIngredientToFood/', AddIngredientToFood.as_view(), name='addIngredientToFood'),
    path('addIngredientToDrink/', AddIngredientToDrink.as_view(), name='addIngredientToDrink'),
    path('acceptRestaurant/<int:restaurant>/', AcceptRestaurant.as_view(), name='acceptRestaurant'),
    path('acceptFood/<int:food>/', AcceptFood.as_view(), name='acceptFood'),
    path('acceptDrink/<int:drink>/', AcceptDrink.as_view(), name='acceptDrink'),
    path('acceptDiet/<int:diet>/', AcceptDiet.as_view(), name='acceptDiet'),
    path('rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls'))
]

urlpatterns += router.urls