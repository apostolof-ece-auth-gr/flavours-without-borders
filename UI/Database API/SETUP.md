# Installation

Welcome to **Hyrieus** installation guide (the API for the application Flavours Without Borders).

**This installation guide is intended for computers using Debian based, Linux distributions!**

The software provided will work on other operating systems too, after loosely following this guide, but changes and decisions may have to be made along the way.

This guide will help you set up the database and Django API required to test the application. Although the guide is pretty detailed don't hesitate to open an issue or contact me directly for any questions.

## Prerequisites

To execute the commands bellow you should have a working MySQL server and support for python 3 and pip 3 on the machine you intend to use.

Other than that the only prerequisite is installation of `virtualenv`, `python3-dev` and `libmysqlclient-dev` packages, using the commands:
```bash
sudo apt install virtualenv
sudo apt-get install python3-dev libmysqlclient-dev
```

If you want the API to be available through the public IP of the computer, you should forward the port `8181`.

## Database and API setup

First you need to uninstall MySQL's password validation plugin. To do this, login to the mysql server as root:
```bash
mysql -h localhost -u root -p
```
and run the following sql command:
```mysql
UNINSTALL PLUGIN validate_password;
```

You are now ready to setup the database and API application using the command bellow and following the instructions:
```bash
./install.sh
```

The script will prompt for an IP address to use, you should input the same IP as the one used earlier.

The script will create the database, create a python 3 virtual environment, install (locally) all necessary python modules, connect the API application to the database and create an API superuser. Lastly the API server will be started.

When the API server is started you can try using the Android app provided or head over to the admin panel or root API page using the commands:
```bash
sensible-browser YOUR_IP:8181/admin/
sensible-browser YOUR_IP:8181/api/
```

## Uninstalling

To revert all changes made to your system follow the steps bellow:

1. Remove Django files created.
```bash
rm -rf hyrieus/env
rm -rf hyrieus/flavoursAPI/hyrieus/migrations
```

2. Revert changes done to the settings.py file by the installation script:
```bash
git checkout -- hyrieus/flavoursAPI/settings.py
```

3. Reinstall MySQL's password validation plugin. Login to the mysql server as root:
```bash
mysql -h localhost -u root -p
```
  and run the following SQL command:
```mysql
INSTALL PLUGIN validate_password SONAME 'validate_password.so';
```

4. Connect to MySQL server and drop the flavoursWithoutBorders schema:
```bash
mysql -h localhost -u root -p -e "DROP DATABASE IF EXISTS flavoursWithoutBorders;"
```