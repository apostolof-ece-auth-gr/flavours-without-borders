USE `flavoursWithoutBorders`;
LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (0,'user','Απλός χρήστης'),(1,'moderator','Διαχειριστής'),(2,'owner','Ιδιοκτήτης');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;