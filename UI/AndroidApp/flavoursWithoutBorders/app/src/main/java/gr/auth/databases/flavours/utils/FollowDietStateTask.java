package gr.auth.databases.flavours.utils;

import android.os.AsyncTask;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static gr.auth.databases.flavours.session.SessionManager.followDietUrl;

public class FollowDietStateTask extends AsyncTask<Integer, Void, Integer> {
    private OkHttpClient client;

    public FollowDietStateTask(OkHttpClient client) {
        this.client = client;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Integer doInBackground(Integer... params) {
        RequestBody requestBody = RequestBody.create(null, new byte[]{});

        //Builds the request
        Request request = new Request.Builder()
                .patch(requestBody)
                .url(followDietUrl + params[0] + "/")
                .build();

        try {
            //Makes request & handles response
            client.newCall(request).execute();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 2;
        }
    }

    @Override
    protected void onPostExecute(Integer result) {
    }
}
