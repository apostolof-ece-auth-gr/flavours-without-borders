package gr.auth.databases.flavours.activities;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.Toolbar;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.base.BaseActivity;
import gr.auth.databases.flavours.model.Ingredient;
import gr.auth.databases.flavours.model.ItemSummary;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;

import static gr.auth.databases.flavours.session.SessionManager.addDrinkUrl;
import static gr.auth.databases.flavours.session.SessionManager.addFoodUrl;

public class AddItemActivity extends BaseActivity {
    public static final String BUNDLE_ADD_ITEM_ITEM_TYPE = "BUNDLE_ADD_ITEM_ITEM_TYPE";
    public static final String BUNDLE_ADD_ITEM_ITEM_RESTAURANT_ID = "BUNDLE_ADD_ITEM_ITEM_RESTAURANT_ID";

    private int restaurantId;
    private ArrayList<Ingredient> ingredients = new ArrayList<>();
    private ItemSummary.ItemType itemType;
    private EditText itemNameInput, itemDescriptionInput;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        Bundle extras = getIntent().getExtras();
        assert extras != null;
        itemType = (ItemSummary.ItemType) extras.getSerializable(BUNDLE_ADD_ITEM_ITEM_TYPE);
        restaurantId = extras.getInt(BUNDLE_ADD_ITEM_ITEM_RESTAURANT_ID);

        Toolbar toolbar = findViewById(R.id.add_item_toolbar);
        if (itemType == ItemSummary.ItemType.FOOD) {
            toolbar.setTitle(getString(R.string.add_item_toolbar_title_food));
        } else if (itemType == ItemSummary.ItemType.DRINK) {
            toolbar.setTitle(getString(R.string.add_item_toolbar_title_drink));
        }

        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        }

        itemNameInput = findViewById(R.id.add_item_name);
        itemDescriptionInput = findViewById(R.id.add_item_description);
        AppCompatButton addItemButton = findViewById(R.id.add_item_add_btn);
        final TextView ingredientsTitle = findViewById(R.id.add_item_add_ingredient_title);
        final EditText addIngredientInput = findViewById(R.id.add_item_add_ingredient);
        final LinearLayout ingredientsList = findViewById(R.id.add_item_ingredients_list);

        if (itemType == ItemSummary.ItemType.FOOD) {
            ((TextInputLayout) itemNameInput.getParent().getParent()).setHint(getString(R.string.add_food_hint_name));
            addItemButton.setText(R.string.add_food_add_btn);
        } else if (itemType == ItemSummary.ItemType.DRINK) {
            ((TextInputLayout) itemNameInput.getParent().getParent()).setHint(getString(R.string.add_drink_hint_name));
            addItemButton.setText(R.string.add_drink_add_btn);
        }

        addIngredientInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    final Ingredient newIngredient = new Ingredient(addIngredientInput.getText().toString(), false);
                    ingredients.add(newIngredient);

                    View ingredientRow = getLayoutInflater().inflate(R.layout.add_item_ingredient_row, ingredientsList, false);
                    TextView ingredientName = ingredientRow.findViewById(R.id.add_item_ingredient_name);
                    ingredientName.setText(ingredients.get(ingredients.size() - 1).getName());

                    ingredientRow.findViewById(R.id.add_item_ingredient_has_alcohol).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ingredients.get(ingredients.indexOf(newIngredient)).
                                    setHasAlcohol(!ingredients.get(ingredients.indexOf(newIngredient)).hasAlcohol());
                        }
                    });

                    AppCompatImageButton removeIngredientBtn = ingredientRow.findViewById(R.id.add_item_remove_ingredient);
                    removeIngredientBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int index = ingredientsList.indexOfChild((View) view.getParent());
                            ingredients.remove(index);
                            ingredientsList.removeViewAt(index);
                        }
                    });

                    ingredientsList.addView(ingredientRow);
                    ingredientsTitle.setVisibility(View.VISIBLE);
                    ingredientsList.setVisibility(View.VISIBLE);

                    addIngredientInput.setText("");
                    return true;
                }
                return false;
            }
        });

        addItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddItemTask addItemTask = new AddItemTask();
                addItemTask.execute();
            }
        });

        createDrawer();
        drawer.setSelection(-1);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            drawer.openDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class AddItemTask extends AsyncTask<Void, Void, Integer> {
        private static final String REQ_FOOD_NAME = "food_name";
        private static final String REQ_FOOD_DESCRIPTION = "food_description";

        private static final String REQ_DRINK_NAME = "drink_name";
        private static final String REQ_DRINK_DESCRIPTION = "drink_description";

        private static final String REQ_ITEM_RESTAURANT = "restaurant";
        private static final String REQ_ITEM_INGREDIENTS = "ingredients";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Void... params) {
            Request request;

            StringBuilder ingredientsJson = new StringBuilder("[");
            for (int ingredientIndex = 0; ingredientIndex < ingredients.size(); ++ingredientIndex) {
                Ingredient ingredient = ingredients.get(ingredientIndex);
                ingredientsJson.append("{\"ingredient_name\": \"").append(ingredient.getName())
                        .append("\", \"ingredient_has_alcohol\": \"").append(ingredient.hasAlcohol())
                        .append("\"}");
                if (ingredientIndex < ingredients.size() - 1) {
                    ingredientsJson.append(",");
                }
            }
            ingredientsJson.append("]");

            if (itemType == ItemSummary.ItemType.FOOD) {
                //Builds the request
                RequestBody formBody = new FormBody.Builder()
                        .add(REQ_FOOD_NAME, itemNameInput.getText().toString())
                        .add(REQ_FOOD_DESCRIPTION, itemDescriptionInput.getText().toString())
                        .add(REQ_ITEM_RESTAURANT, "" + restaurantId)
                        .add(REQ_ITEM_INGREDIENTS, ingredientsJson.toString())
                        .build();
                request = new Request.Builder()
                        .url(addFoodUrl)
                        .post(formBody)
                        .build();
            } else {
                //Builds the request
                RequestBody formBody = new FormBody.Builder()
                        .add(REQ_DRINK_NAME, itemNameInput.getText().toString())
                        .add(REQ_DRINK_DESCRIPTION, itemDescriptionInput.getText().toString())
                        .add(REQ_ITEM_RESTAURANT, "" + restaurantId)
                        .add(REQ_ITEM_INGREDIENTS, ingredientsJson.toString())
                        .build();
                request = new Request.Builder()
                        .url(addDrinkUrl)
                        .post(formBody)
                        .build();
            }

            try {
                //Makes request & handles response
                client.newCall(request).execute();

                return 0;

            } catch (Exception e) {
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            Toast.makeText(AddItemActivity.this, (itemType == ItemSummary.ItemType.FOOD ? "Food" : "Drink")
                    + " was added!", Toast.LENGTH_SHORT).show();
            AddItemActivity.this.finish();
        }
    }
}