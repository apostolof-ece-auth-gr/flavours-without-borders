package gr.auth.databases.flavours.model;

import android.os.Parcel;

import androidx.annotation.Nullable;

public class RestaurantRating extends Rating {
    public enum Accessibility {
        EASY(0), MODERATE(1), HARD(2), UNSUPPORTED(-1);

        private int id;

        Accessibility(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        @Nullable
        public static String getWorkingTypeFromId(int id) {
            switch (id) {
                case 0:
                    return "Easy";
                case 1:
                    return "Moderate";
                case 2:
                    return "Hard";
                default:
                    return null;
            }
        }

        public static Accessibility getEnumTypeFromId(int id) {
            switch (id) {
                case 0:
                    return EASY;
                case 1:
                    return MODERATE;
                case 2:
                    return HARD;
                default:
                    return UNSUPPORTED;
            }
        }

        public static Accessibility getEnumTypeFromString(String possible) {
            switch (possible.toLowerCase()) {
                case "easy":
                    return EASY;
                case "moderate":
                    return MODERATE;
                case "hard":
                    return HARD;
                default:
                    return UNSUPPORTED;
            }
        }
    }

    private Accessibility accessibility;
    private String diet;

    public RestaurantRating(int grade, int userID, String username, String text, String date, Accessibility accessibility, String diet) {
        super(grade, userID, username, text, date);
        this.accessibility = accessibility;
        this.diet = diet;
    }

    public Accessibility getAccessibility() {
        return accessibility;
    }

    public String getDiet() {
        return diet;
    }

    private RestaurantRating(Parcel in) {
        super(in);
        accessibility = Accessibility.getEnumTypeFromId(in.readInt());
        diet = in.readString();
    }

    public static final Creator<RestaurantRating> CREATOR = new Creator<RestaurantRating>() {
        @Override
        public RestaurantRating createFromParcel(Parcel in) {
            return new RestaurantRating(in);
        }

        @Override
        public RestaurantRating[] newArray(int size) {
            return new RestaurantRating[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(accessibility.getId());
        dest.writeString(diet);
    }

    public int describeContents() {
        return 0;
    }
}
