package gr.auth.databases.flavours.model;

import android.os.Parcel;
import android.os.Parcelable;

public class RestaurantSummary implements Parcelable {
    private int id;
    private String name;

    public RestaurantSummary(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(name);
    }

    public static final Parcelable.Creator<RestaurantSummary> CREATOR = new Parcelable.Creator<RestaurantSummary>() {
        public RestaurantSummary createFromParcel(Parcel in) {
            return new RestaurantSummary(in);
        }

        public RestaurantSummary[] newArray(int size) {
            return new RestaurantSummary[size];
        }
    };

    private RestaurantSummary(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }
}
