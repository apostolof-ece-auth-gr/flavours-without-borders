package gr.auth.databases.flavours.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

public class Restaurant implements Parcelable {
    public enum RestaurantType {
        CAFETERIA(0), PUB(1), BAR(2), RESTAURANT(3), FAST_FOOD(4), ETHNIC(5), UNSUPPORTED(-1);

        private int id;

        RestaurantType(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        @Nullable
        public static String getWorkingTypeFromId(int id) {
            switch (id) {
                case 0:
                    return "Cafeteria";
                case 1:
                    return "Pub";
                case 2:
                    return "Bar";
                case 3:
                    return "Restaurant";
                case 4:
                    return "Fast Food";
                case 5:
                    return "Ethnic";
                default:
                    return null;
            }
        }

        public static RestaurantType getEnumTypeFromId(int id) {
            switch (id) {
                case 0:
                    return CAFETERIA;
                case 1:
                    return PUB;
                case 2:
                    return BAR;
                case 3:
                    return RESTAURANT;
                case 4:
                    return FAST_FOOD;
                case 5:
                    return ETHNIC;
                default:
                    return UNSUPPORTED;
            }
        }
    }

    private int id;
    private String name, type, openingTime, closingTime;
    private double longitude, latitude;
    private boolean isAccepted;

    public Restaurant(int id, String name, String type, double longitude, double latitude,
                      String openingTime, String closingTime, boolean isAccepted) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.longitude = longitude;
        this.latitude = latitude;
        this.openingTime = openingTime;
        this.closingTime = closingTime;
        this.isAccepted = isAccepted;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean accepted) {
        isAccepted = accepted;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(name);
        out.writeString(type);
        out.writeDouble(longitude);
        out.writeDouble(latitude);
        out.writeString(openingTime);
        out.writeString(closingTime);
        out.writeByte((byte) (isAccepted ? 1 : 0));
    }

    public static final Parcelable.Creator<Restaurant> CREATOR = new Parcelable.Creator<Restaurant>() {
        public Restaurant createFromParcel(Parcel in) {
            return new Restaurant(in);
        }

        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };

    Restaurant(Parcel in) {
        id = in.readInt();
        name = in.readString();
        type = in.readString();
        longitude = in.readDouble();
        latitude = in.readDouble();
        openingTime = in.readString();
        closingTime = in.readString();
        isAccepted = in.readByte() != 0;
    }
}
