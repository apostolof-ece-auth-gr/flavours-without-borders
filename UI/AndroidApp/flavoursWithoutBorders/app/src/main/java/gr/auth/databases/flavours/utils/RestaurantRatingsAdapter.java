package gr.auth.databases.flavours.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.model.RestaurantRating;

public class RestaurantRatingsAdapter extends RecyclerView.Adapter<RestaurantRatingsAdapter.RatingViewHolder> {
    private Context context;
    private ArrayList<RestaurantRating> ratings;
    private ProfileRestaurantRatingsAdapterInteractionListener profileRestaurantRatingsAdapterInteractionListener;

    public RestaurantRatingsAdapter(@NonNull Context context, ArrayList<RestaurantRating> ratings,
                                    ProfileRestaurantRatingsAdapterInteractionListener profileRestaurantRatingsAdapterInteractionListener) {
        this.context = context;
        this.ratings = ratings;
        this.profileRestaurantRatingsAdapterInteractionListener = profileRestaurantRatingsAdapterInteractionListener;
    }

    @NonNull
    @Override
    public RestaurantRatingsAdapter.RatingViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                                        int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.restaurant_rating_row,
                parent, false);
        return new RatingViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final RatingViewHolder holder, int position) {
        holder.username.setText(ratings.get(position).getUsername());
        if (profileRestaurantRatingsAdapterInteractionListener != null) {
            holder.username.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    profileRestaurantRatingsAdapterInteractionListener.
                            onProfileRestaurantRatingsAdapterInteraction(
                                    ratings.get(holder.getAdapterPosition()).getUserID(),
                                    ratings.get(holder.getAdapterPosition()).getUsername());
                }
            });
        }
        holder.date.setText(ratings.get(position).getDate());

        holder.grade.setText(context.getString(R.string.restaurant_ratings_row_grade_placeholder,
                ratings.get(position).getGrade()));

        if (ratings.get(position).getAccessibility() != null &&
                ratings.get(position).getAccessibility() != RestaurantRating.Accessibility.UNSUPPORTED) {
            holder.accessibility.setVisibility(View.VISIBLE);
            holder.accessibility.setText(context.getString(R.string.restaurant_ratings_row_accessibility_placeholder,
                    ratings.get(position).getAccessibility().toString()));
        } else {
            holder.accessibility.setVisibility(View.GONE);
        }

        if (!ratings.get(position).getDiet().equals("null")) {
            holder.diet.setVisibility(View.VISIBLE);
            holder.diet.setText(context.getString(R.string.restaurant_ratings_row_diet_placeholder,
                    ratings.get(position).getDiet()));
        } else {
            holder.diet.setVisibility(View.GONE);
        }

        if (!ratings.get(position).getText().equals("null")) {
            holder.text.setVisibility(View.VISIBLE);
            holder.text.setText(ratings.get(position).getText());
        } else {
            holder.text.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return ratings == null ? 0 : ratings.size();
    }

    static class RatingViewHolder extends RecyclerView.ViewHolder {
        TextView username, date, grade, accessibility, diet, text;

        RatingViewHolder(View v) {
            super(v);
            username = v.findViewById(R.id.restaurant_rating_username);
            date = v.findViewById(R.id.restaurant_rating_date);
            grade = v.findViewById(R.id.restaurant_rating_grade);
            accessibility = v.findViewById(R.id.restaurant_rating_accessibility);
            diet = v.findViewById(R.id.restaurant_rating_diet);
            text = v.findViewById(R.id.restaurant_rating_text);
        }
    }

    public interface ProfileRestaurantRatingsAdapterInteractionListener {
        void onProfileRestaurantRatingsAdapterInteraction(int profileID, String profileUsername);
    }
}