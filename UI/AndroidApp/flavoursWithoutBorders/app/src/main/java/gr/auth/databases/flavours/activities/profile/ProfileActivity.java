package gr.auth.databases.flavours.activities.profile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.activities.profile.fragments.ProfileDrinkRatingsFragment;
import gr.auth.databases.flavours.activities.profile.fragments.ProfileFoodRatingsFragment;
import gr.auth.databases.flavours.activities.profile.fragments.ProfileInfoFragment;
import gr.auth.databases.flavours.activities.profile.fragments.ProfileRestaurantRatingsFragment;
import gr.auth.databases.flavours.base.BaseActivity;
import gr.auth.databases.flavours.model.DietSummary;
import gr.auth.databases.flavours.model.IngredientSummary;
import gr.auth.databases.flavours.model.ItemRating;
import gr.auth.databases.flavours.model.Profile;
import gr.auth.databases.flavours.model.RestaurantRating;
import gr.auth.databases.flavours.model.RestaurantSummary;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static gr.auth.databases.flavours.session.SessionManager.profileUserViewUrl;

public class ProfileActivity extends BaseActivity {
    public static final String BUNDLE_USER_ID = "BUNDLE_USER_ID";
    public static final String BUNDLE_USER_NAME = "BUNDLE_USER_NAME";
    private ViewPager viewPager;

    private int profileID;
    private Profile profileUserView;
    private ArrayList<ItemRating> drinkRatings = new ArrayList<>();
    private ArrayList<ItemRating> foodRatings = new ArrayList<>();
    private ArrayList<RestaurantRating> restaurantRatings = new ArrayList<>();
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Bundle extras = getIntent().getExtras();
        assert extras != null;
        profileID = extras.getInt(BUNDLE_USER_ID);
        String username = extras.getString(BUNDLE_USER_NAME);

        Toolbar toolbar = findViewById(R.id.profile_toolbar);
        toolbar.setTitle(username);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        }

        createDrawer();

        viewPager = findViewById(R.id.profile_pager);
        tabLayout = findViewById(R.id.profile_tabs);

        ProfileTask profileTask = new ProfileTask();
        profileTask.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            drawer.openDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        RestaurantPagerAdapter adapter = new RestaurantPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(ProfileInfoFragment.newInstance(profileUserView, profileID == sessionManager.getUserId()), "INFO");
        adapter.addFrag(ProfileFoodRatingsFragment.newInstance(foodRatings), "FOODS");
        adapter.addFrag(ProfileDrinkRatingsFragment.newInstance(drinkRatings), "DRINKS");
        adapter.addFrag(ProfileRestaurantRatingsFragment.newInstance(restaurantRatings), "PLACES");
        viewPager.setAdapter(adapter);
    }

    private class RestaurantPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        RestaurantPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private class ProfileTask extends AsyncTask<Void, Void, Integer> {
        private static final String JSON_TAG_PROFILE_INFO = "userInfo";
        private static final String JSON_TAG_PROFILE_USERNAME = "username";
        private static final String JSON_TAG_PROFILE_EMAIL = "email";
        private static final String JSON_TAG_PROFILE_AGE = "age";
        private static final String JSON_TAG_PROFILE_REVIEWS_NUMBER = "reviewsNumber";

        private static final String JSON_TAG_PROFILE_RESTAURANTS_OWNED = "owns";
        private static final String JSON_TAG_PROFILE_RESTAURANT_OWNED_ID = "restaurant_id";
        private static final String JSON_TAG_PROFILE_RESTAURANT_OWNED_NAME = "restaurant_name";

        private static final String JSON_TAG_PROFILE_DIETS_FOLLOWED = "diets";
        private static final String JSON_TAG_PROFILE_DIET_FOLLOWED_ID = "diet_id";
        private static final String JSON_TAG_PROFILE_DIET_FOLLOWED_NAME = "diet_name";

        private static final String JSON_TAG_PROFILE_INGREDIENTS_PROHIBITED = "ingredients";
        private static final String JSON_TAG_PROFILE_INGREDIENTS_PROHIBITED_NAME = "ingredient_name";

        private static final String JSON_TAG_FOOD_RATINGS = "foodRatings";
        private static final String JSON_TAG_FOOD_RATING_GRADE = "rating_grade";
        private static final String JSON_TAG_FOOD_RATING_USER_ID = "user_id";
        private static final String JSON_TAG_FOOD_RATING_USERNAME = "username";
        private static final String JSON_TAG_FOOD_RATING_TEXT = "rating_text";
        private static final String JSON_TAG_FOOD_RATING_DATE = "rating_date";
        private static final String JSON_TAG_FOOD_RATING_PORTION_SIZE = "rating_pοrtion_size";

        private static final String JSON_TAG_DRINK_RATINGS = "drinkRatings";
        private static final String JSON_TAG_DRINK_RATING_GRADE = "rating_grade";
        private static final String JSON_TAG_DRINK_RATING_USER_ID = "user_id";
        private static final String JSON_TAG_DRINK_RATING_USERNAME = "username";
        private static final String JSON_TAG_DRINK_RATING_TEXT = "rating_text";
        private static final String JSON_TAG_DRINK_RATING_DATE = "rating_date";
        private static final String JSON_TAG_DRINK_RATING_PORTION_SIZE = "rating_pοrtion_size";

        private static final String JSON_TAG_RESTAURANT_RATINGS = "restaurantRatings";
        private static final String JSON_TAG_RESTAURANT_RATING_GRADE = "rating_grade";
        private static final String JSON_TAG_RESTAURANT_RATING_USER_ID = "user_id";
        private static final String JSON_TAG_RESTAURANT_RATING_USERNAME = "username";
        private static final String JSON_TAG_RESTAURANT_RATING_TEXT = "rating_text";
        private static final String JSON_TAG_RESTAURANT_RATING_DATE = "rating_date";
        private static final String JSON_TAG_RESTAURANT_RATING_ACCESSIBILITY = "rating_accessibility";
        private static final String JSON_TAG_RESTAURANT_RATING_DIET = "diet_name";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Void... params) {
            String requestUrl = profileUserViewUrl + profileID + "/";

            //Builds the request
            Request request = new Request.Builder()
                    .url(requestUrl)
                    .build();

            try {
                //Makes request & handles response
                Response response = client.newCall(request).execute();

                ResponseBody responseBody = response.body();
                assert responseBody != null;
                String result = responseBody.string();
                JSONObject jsonResponse = new JSONObject(result);

                JSONObject jsonProfileInfo = jsonResponse.getJSONObject(JSON_TAG_PROFILE_INFO);

                ArrayList<RestaurantSummary> restaurantsOwned = new ArrayList<>();

                JSONArray jsonRestaurantsOwned = jsonProfileInfo.getJSONArray(JSON_TAG_PROFILE_RESTAURANTS_OWNED);
                for (int restaurantIndex = 0; restaurantIndex < jsonRestaurantsOwned.length(); ++restaurantIndex) {
                    JSONObject restaurantOwned = jsonRestaurantsOwned.getJSONObject(restaurantIndex);
                    restaurantsOwned.add(new RestaurantSummary(restaurantOwned.getInt(JSON_TAG_PROFILE_RESTAURANT_OWNED_ID),
                            restaurantOwned.getString(JSON_TAG_PROFILE_RESTAURANT_OWNED_NAME)));
                }

                ArrayList<DietSummary> dietsFollowed = new ArrayList<>();

                JSONArray jsonDietsFollowed = jsonProfileInfo.getJSONArray(JSON_TAG_PROFILE_DIETS_FOLLOWED);
                for (int dietIndex = 0; dietIndex < jsonDietsFollowed.length(); ++dietIndex) {
                    JSONObject jsonDiet = jsonDietsFollowed.getJSONObject(dietIndex);
                    dietsFollowed.add(new DietSummary(jsonDiet.getInt(JSON_TAG_PROFILE_DIET_FOLLOWED_ID),
                            jsonDiet.getString(JSON_TAG_PROFILE_DIET_FOLLOWED_NAME)));
                }

                ArrayList<IngredientSummary> ingredientsProhibited = new ArrayList<>();

                JSONArray jsonIngredientsProhibited = jsonProfileInfo.getJSONArray(JSON_TAG_PROFILE_INGREDIENTS_PROHIBITED);
                for (int ingredientIndex = 0; ingredientIndex < jsonIngredientsProhibited.length(); ++ingredientIndex) {
                    JSONObject jsonIngredient = jsonIngredientsProhibited.getJSONObject(ingredientIndex);
                    ingredientsProhibited.add(new IngredientSummary(jsonIngredient.getString(JSON_TAG_PROFILE_INGREDIENTS_PROHIBITED_NAME)));
                }

                profileUserView = new Profile(profileID,
                        jsonProfileInfo.getInt(JSON_TAG_PROFILE_AGE),
                        jsonProfileInfo.getInt(JSON_TAG_PROFILE_REVIEWS_NUMBER),
                        jsonProfileInfo.getString(JSON_TAG_PROFILE_USERNAME),
                        jsonProfileInfo.getString(JSON_TAG_PROFILE_EMAIL),
                        restaurantsOwned,
                        dietsFollowed,
                        ingredientsProhibited);

                JSONArray jsonFoodRatings = jsonResponse.getJSONArray(JSON_TAG_FOOD_RATINGS);
                for (int ratingIndex = 0; ratingIndex < jsonFoodRatings.length(); ++ratingIndex) {
                    JSONObject rating = jsonFoodRatings.getJSONObject(ratingIndex);
                    foodRatings.add(new ItemRating(rating.getInt(JSON_TAG_FOOD_RATING_GRADE),
                            rating.getInt(JSON_TAG_FOOD_RATING_USER_ID),
                            rating.getString(JSON_TAG_FOOD_RATING_USERNAME),
                            rating.getString(JSON_TAG_FOOD_RATING_TEXT),
                            rating.getString(JSON_TAG_FOOD_RATING_DATE),
                            ItemRating.PortionSize.getEnumTypeFromString(
                                    rating.getString(JSON_TAG_FOOD_RATING_PORTION_SIZE))));
                }

                JSONArray jsonDrinkRatings = jsonResponse.getJSONArray(JSON_TAG_DRINK_RATINGS);
                for (int ratingIndex = 0; ratingIndex < jsonDrinkRatings.length(); ++ratingIndex) {
                    JSONObject rating = jsonDrinkRatings.getJSONObject(ratingIndex);
                    drinkRatings.add(new ItemRating(rating.getInt(JSON_TAG_DRINK_RATING_GRADE),
                            rating.getInt(JSON_TAG_DRINK_RATING_USER_ID),
                            rating.getString(JSON_TAG_DRINK_RATING_USERNAME),
                            rating.getString(JSON_TAG_DRINK_RATING_TEXT),
                            rating.getString(JSON_TAG_DRINK_RATING_DATE),
                            ItemRating.PortionSize.getEnumTypeFromString(
                                    rating.getString(JSON_TAG_DRINK_RATING_PORTION_SIZE))));
                }

                JSONArray jsonRestaurantRatings = jsonResponse.getJSONArray(JSON_TAG_RESTAURANT_RATINGS);
                for (int ratingIndex = 0; ratingIndex < jsonRestaurantRatings.length(); ++ratingIndex) {
                    JSONObject rating = jsonRestaurantRatings.getJSONObject(ratingIndex);
                    restaurantRatings.add(new RestaurantRating(rating.getInt(JSON_TAG_RESTAURANT_RATING_GRADE),
                            rating.getInt(JSON_TAG_RESTAURANT_RATING_USER_ID),
                            rating.getString(JSON_TAG_RESTAURANT_RATING_USERNAME),
                            rating.getString(JSON_TAG_RESTAURANT_RATING_TEXT),
                            rating.getString(JSON_TAG_RESTAURANT_RATING_DATE),
                            RestaurantRating.Accessibility.getEnumTypeFromString(
                                    rating.getString(JSON_TAG_RESTAURANT_RATING_ACCESSIBILITY)),
                            rating.getString(JSON_TAG_RESTAURANT_RATING_DIET)));
                }

                return 1;
            } catch (Exception e) {
                e.printStackTrace();
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            setupViewPager(viewPager);
            tabLayout.setupWithViewPager(viewPager);
        }
    }
}
