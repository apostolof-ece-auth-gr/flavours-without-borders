package gr.auth.databases.flavours.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Drink implements Parcelable {
    private int id;
    private String name, restaurantName, description;
    private boolean hasAlcohol;
    private double rating;

    public Drink(int id, String name, String restaurantName, String description, boolean hasAlcohol, double rating) {
        this.id = id;
        this.name = name;
        this.restaurantName = restaurantName;
        this.description = description;
        this.hasAlcohol = hasAlcohol;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public String getDescription() {
        return description;
    }

    public boolean hasAlcohol() {
        return hasAlcohol;
    }

    public double getRating() {
        return rating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(name);
        out.writeString(restaurantName);
        out.writeString(description);
        out.writeByte((byte) (hasAlcohol ? 1 : 0));
        out.writeDouble(rating);
    }

    public static final Parcelable.Creator<Drink> CREATOR = new Parcelable.Creator<Drink>() {
        public Drink createFromParcel(Parcel in) {
            return new Drink(in);
        }

        public Drink[] newArray(int size) {
            return new Drink[size];
        }
    };

    private Drink(Parcel in) {
        id = in.readInt();
        name = in.readString();
        restaurantName = in.readString();
        description = in.readString();
        hasAlcohol = in.readByte() != 0;
        rating = in.readDouble();
    }
}
