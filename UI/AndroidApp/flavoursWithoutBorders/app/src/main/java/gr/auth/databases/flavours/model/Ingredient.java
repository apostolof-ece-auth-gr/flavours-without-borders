package gr.auth.databases.flavours.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Ingredient implements Parcelable {
    private String name;
    private boolean hasAlcohol;

    public Ingredient(String name, boolean hasAlcohol) {
        this.name = name;
        this.hasAlcohol = hasAlcohol;
    }

    public String getName() {
        return name;
    }

    public boolean hasAlcohol() {
        return hasAlcohol;
    }

    public void setHasAlcohol(boolean hasAlcohol) {
        this.hasAlcohol = hasAlcohol;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(name);
        out.writeByte((byte) (hasAlcohol ? 1 : 0));
    }

    public static final Parcelable.Creator<Ingredient> CREATOR = new Parcelable.Creator<Ingredient>() {
        public Ingredient createFromParcel(Parcel in) {
            return new Ingredient(in);
        }

        public Ingredient[] newArray(int size) {
            return new Ingredient[size];
        }
    };

    private Ingredient(Parcel in) {
        name = in.readString();
        hasAlcohol = in.readByte() != 0;
    }
}