package gr.auth.databases.flavours.activities.profile.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.base.BaseApplication;
import gr.auth.databases.flavours.model.DietSummary;
import gr.auth.databases.flavours.model.IngredientSummary;
import gr.auth.databases.flavours.model.Profile;
import gr.auth.databases.flavours.utils.FollowDietStateTask;
import gr.auth.databases.flavours.utils.ProhibitIngredientStateTask;

public class ProfileInfoFragment extends Fragment {

    public ProfileInfoFragment() {
        // Required empty public constructor
    }

    private static final String PROFILE_INFO = "PROFILE_INFO";
    private static final String PROFILE_IS_ME = "PROFILE_IS_ME";

    private Profile profileUserView;
    private boolean isMe = false;

    public static ProfileInfoFragment newInstance(Profile profileUserView, boolean isMe) {
        ProfileInfoFragment fragment = new ProfileInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(PROFILE_INFO, profileUserView);
        args.putBoolean(PROFILE_IS_ME, isMe);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        profileUserView = getArguments().getParcelable(PROFILE_INFO);
        isMe = getArguments().getBoolean(PROFILE_IS_ME);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_profile_info, container, false);

        TextView email = rootView.findViewById(R.id.profile_email);
        if (!profileUserView.getEmail().equals("null")) {
            email.setText(getString(R.string.profile_email_placeholder, profileUserView.getEmail()));
        } else {
            email.setVisibility(View.GONE);
        }

        TextView age = rootView.findViewById(R.id.profile_age);
        if (profileUserView.getAge() != -1) {
            age.setText(getString(R.string.profile_age_placeholder, profileUserView.getAge()));
        } else {
            age.setVisibility(View.GONE);
        }

        TextView numberOfRatings = rootView.findViewById(R.id.profile_number_of_reviews);
        numberOfRatings.setText(getString(R.string.profile_number_of_reviews_placeholder, profileUserView.getReviewsNumber()));

        TextView ownsRestaurant = rootView.findViewById(R.id.profile_owns_restaurant);
        if (!profileUserView.getRestaurantsOwned().isEmpty()) {
            StringBuilder restaurantsOwned = new StringBuilder();
            for (int restaurantIndex = 0; restaurantIndex < profileUserView.getRestaurantsOwned().size(); ++restaurantIndex) {
                restaurantsOwned.append(profileUserView.getRestaurantsOwned().get(restaurantIndex).getName());
                if (restaurantIndex != profileUserView.getRestaurantsOwned().size() - 1) {
                    restaurantsOwned.append(", ");
                }
            }
            ownsRestaurant.setText(getString(R.string.profile_owns_restaurant_placeholder, restaurantsOwned.toString()));
            ownsRestaurant.setVisibility(View.VISIBLE);
        }

        if (!profileUserView.getDietsFollowed().isEmpty()) {
            (rootView.findViewById(R.id.profile_diets_list_title)).setVisibility(View.VISIBLE);

            final LinearLayout dietsList = rootView.findViewById(R.id.profile_diets_list);
            dietsList.setVisibility(View.VISIBLE);

            for (final DietSummary dietFollowed : profileUserView.getDietsFollowed()) {
                final View userDietRow = getLayoutInflater().inflate(R.layout.profile_diet_row, dietsList, false);

                TextView dietName = userDietRow.findViewById(R.id.profile_diet_name);
                dietName.setText(dietFollowed.getName());

                AppCompatImageButton removeDiet = userDietRow.findViewById(R.id.profile_diet_remove);
                if (isMe) {
                    removeDiet.setVisibility(View.VISIBLE);
                    removeDiet.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            FollowDietStateTask followDietStateTask = new FollowDietStateTask(BaseApplication.getInstance().getClient());
                            followDietStateTask.execute(dietFollowed.getId());
                            dietsList.removeView(userDietRow);
                        }
                    });
                }

                dietsList.addView(userDietRow);
            }
        }

        if (!profileUserView.getIngredientsProhibited().isEmpty()) {
            (rootView.findViewById(R.id.profile_ingredients_list_title)).setVisibility(View.VISIBLE);

            final LinearLayout ingredientsList = rootView.findViewById(R.id.profile_ingredients_list);
            ingredientsList.setVisibility(View.VISIBLE);

            for (final IngredientSummary ingredientProhibited : profileUserView.getIngredientsProhibited()) {
                final View userIngredientRow = getLayoutInflater().inflate(R.layout.profile_ingredient_row, ingredientsList, false);

                TextView ingredientName = userIngredientRow.findViewById(R.id.profile_ingredient_name);
                ingredientName.setText(ingredientProhibited.getName());

                AppCompatImageButton removeDiet = userIngredientRow.findViewById(R.id.profile_ingredient_remove);
                if (isMe) {
                    removeDiet.setVisibility(View.VISIBLE);
                    removeDiet.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ProhibitIngredientStateTask prohibitIngredientStateTask = new ProhibitIngredientStateTask(BaseApplication.getInstance().getClient());
                            prohibitIngredientStateTask.execute(ingredientProhibited.getName());
                            ingredientsList.removeView(userIngredientRow);
                        }
                    });
                }

                ingredientsList.addView(userIngredientRow);
            }
        }

        return rootView;
    }
}
