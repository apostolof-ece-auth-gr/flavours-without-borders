package gr.auth.databases.flavours.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Food implements Parcelable {
    private int id, calories;
    private String name, restaurantName, description;
    private double rating;

    public Food(int id, String name, String restaurantName, String description, int calories, double rating) {
        this.id = id;
        this.name = name;
        this.restaurantName = restaurantName;
        this.description = description;
        this.calories = calories;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public String getDescription() {
        return description;
    }

    public int getCalories() {
        return calories;
    }

    public double getRating() {
        return rating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(name);
        out.writeString(restaurantName);
        out.writeString(description);
        out.writeInt(calories);
        out.writeDouble(rating);
    }

    public static final Parcelable.Creator<Food> CREATOR = new Parcelable.Creator<Food>() {
        public Food createFromParcel(Parcel in) {
            return new Food(in);
        }

        public Food[] newArray(int size) {
            return new Food[size];
        }
    };

    private Food(Parcel in) {
        id = in.readInt();
        name = in.readString();
        restaurantName = in.readString();
        description = in.readString();
        calories = in.readInt();
        rating = in.readDouble();
    }
}
