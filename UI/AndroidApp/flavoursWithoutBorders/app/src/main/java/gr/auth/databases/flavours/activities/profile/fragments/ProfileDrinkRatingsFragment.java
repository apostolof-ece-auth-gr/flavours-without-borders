package gr.auth.databases.flavours.activities.profile.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.model.ItemRating;
import gr.auth.databases.flavours.utils.ItemRatingsAdapter;

public class ProfileDrinkRatingsFragment extends Fragment {

    public ProfileDrinkRatingsFragment() {
        // Required empty public constructor
    }

    private static final String PROFILE_DRINK_RATINGS = "PROFILE_DRINK_RATINGS";

    private ArrayList<ItemRating> drinkRatings;

    public static ProfileDrinkRatingsFragment newInstance(ArrayList<ItemRating> drinkRatings) {
        ProfileDrinkRatingsFragment fragment = new ProfileDrinkRatingsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(PROFILE_DRINK_RATINGS, drinkRatings);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        drinkRatings = getArguments().getParcelableArrayList(PROFILE_DRINK_RATINGS);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.reusable_recycler_list, container, false);

        Context context = getContext();
        assert context != null;
        ItemRatingsAdapter itemAdapter = new ItemRatingsAdapter(context, drinkRatings, null);
        RecyclerView mainContent = rootView.findViewById(R.id.recycler_list);
        mainContent.setAdapter(itemAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mainContent.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mainContent.getContext(),
                layoutManager.getOrientation());
        mainContent.addItemDecoration(dividerItemDecoration);

        return rootView;
    }
}
