package gr.auth.databases.flavours.activities.drink.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.model.Drink;

public class DrinkInfoFragment extends Fragment {

    public DrinkInfoFragment() {
        // Required empty public constructor
    }

    private static final String DRINK_INFO = "DRINK_INFO";

    private Drink drink;

    public static DrinkInfoFragment newInstance(Drink drink) {
        DrinkInfoFragment fragment = new DrinkInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(DRINK_INFO, drink);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        drink = getArguments().getParcelable(DRINK_INFO);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_drink_info, container, false);

        TextView restaurantServing = rootView.findViewById(R.id.drink_serving_restaurant);
        restaurantServing.setText(getString(R.string.drink_serving_restaurant_placeholder, drink.getRestaurantName()));
        TextView hasAlcohol = rootView.findViewById(R.id.drink_contains_alcohol);
        hasAlcohol.setText(getString(R.string.drink_has_alcohol_placeholder, drink.hasAlcohol() ? "YES" : "NO"));
        TextView rating = rootView.findViewById(R.id.drink_rating);
        if (drink.getRating() != -1) {
            rating.setText(getString(R.string.drink_rating_placeholder, drink.getRating()));
        } else {
            rating.setVisibility(View.GONE);
        }
        TextView description = rootView.findViewById(R.id.drink_description);
        if (!drink.getDescription().equals("null")) {
            description.setText(drink.getDescription());
        } else {
            description.setVisibility(View.GONE);
        }

        return rootView;
    }
}
