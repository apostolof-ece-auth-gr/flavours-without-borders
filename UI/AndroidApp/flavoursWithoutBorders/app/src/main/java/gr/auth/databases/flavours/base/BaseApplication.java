package gr.auth.databases.flavours.base;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.DisplayMetrics;

import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;
import gr.auth.databases.flavours.session.SessionManager;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static gr.auth.databases.flavours.base.BaseActivity.PREF_BASE_SERVER_IP;

public class BaseApplication extends Application {
    public static final String CSRF_TOKEN = "CSRF_TOKEN";

    private static BaseApplication baseApplication; //BaseApplication singleton

    //Client & SessionManager
    private OkHttpClient client;
    private SessionManager sessionManager;

    private static final String SHARED_PREFS = "FlavoursSharedPrefs";

    private static float dpWidth;

    public static BaseApplication getInstance() {
        return baseApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        baseApplication = this; //init singleton

        //Shared Preferences
        final SharedPreferences sharedPrefs = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);

        SharedPrefsCookiePersistor sharedPrefsCookiePersistor = new SharedPrefsCookiePersistor(getApplicationContext());
        PersistentCookieJar cookieJar = new PersistentCookieJar(new SetCookieCache(), sharedPrefsCookiePersistor);
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .cookieJar(cookieJar)
                .connectTimeout(40, TimeUnit.SECONDS)
                .writeTimeout(40, TimeUnit.SECONDS)
                .readTimeout(40, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @NonNull
                    @Override
                    public Response intercept(@NonNull Chain chain) throws IOException {
                        final Request original = chain.request();

                        String csrfToken = sharedPrefs.getString(CSRF_TOKEN, "too soon to have a token");
                        assert csrfToken != null;
                        final Request authorized = original.newBuilder()
                                .addHeader("X-CSRFToken", csrfToken)
                                .build();
                        return chain.proceed(authorized);

                    }
                });

        client = builder.build();


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String baseServerIP = sharedPreferences.getString(PREF_BASE_SERVER_IP, null);
        sessionManager = new SessionManager(client, cookieJar, sharedPrefs, baseServerIP);

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        dpWidth = displayMetrics.widthPixels / displayMetrics.density;
    }

    //Getters
    public Context getContext() {
        return getApplicationContext();
    }

    public OkHttpClient getClient() {
        return client;
    }

    public SessionManager getSessionManager() {
        return sessionManager;
    }

    public float getDpWidth() {
        return dpWidth;
    }
}
