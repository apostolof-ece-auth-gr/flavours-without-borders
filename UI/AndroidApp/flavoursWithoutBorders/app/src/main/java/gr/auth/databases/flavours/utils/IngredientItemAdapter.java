package gr.auth.databases.flavours.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.model.Ingredient;

public class IngredientItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final ArrayList<Ingredient> ingredients;
    private Context context;

    public IngredientItemAdapter(@NonNull Context context, ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.ingredient_row, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        Ingredient ingredient = ingredients.get(position);
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        itemViewHolder.name.setText(ingredient.getName());
        itemViewHolder.hasAlcohol.setText(context.getString(R.string.ingredient_has_alcohol_placeholder,
                ingredient.hasAlcohol() ? "YES" : "NO"));
    }

    @Override
    public int getItemCount() {
        return ingredients == null ? 0 : ingredients.size();
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView name, hasAlcohol;

        ItemViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.ingredient_name);
            hasAlcohol = itemView.findViewById(R.id.ingredient_has_alcohol);
        }
    }
}
