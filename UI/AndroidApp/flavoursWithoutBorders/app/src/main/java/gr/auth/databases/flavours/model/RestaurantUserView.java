package gr.auth.databases.flavours.model;

import android.os.Parcel;

import java.util.ArrayList;

public class RestaurantUserView extends Restaurant {
    private double averageRating;
    private ArrayList<DietRatingPair> averageRatingPerDiet;

    public RestaurantUserView(int id, String name, String type, double longitude, double latitude,
                              String openingTime, String closingTime, boolean isAccepted, double averageRating,
                              ArrayList<DietRatingPair> averageRatingPerDiet) {
        super(id, name, type, longitude, latitude, openingTime, closingTime, isAccepted);
        this.averageRating = averageRating;
        this.averageRatingPerDiet = averageRatingPerDiet;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public ArrayList<DietRatingPair> getAverageRatingPerDiet() {
        return averageRatingPerDiet;
    }

    private RestaurantUserView(Parcel in) {
        super(in);
        averageRating = in.readDouble();
        averageRatingPerDiet = (ArrayList<DietRatingPair>) in.readArrayList(DietRatingPair.class.getClassLoader());
    }

    public static final Creator<RestaurantUserView> CREATOR = new Creator<RestaurantUserView>() {
        @Override
        public RestaurantUserView createFromParcel(Parcel in) {
            return new RestaurantUserView(in);
        }

        @Override
        public RestaurantUserView[] newArray(int size) {
            return new RestaurantUserView[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeDouble(averageRating);
        //dest.writeList(averageRatingPerDiet);
        dest.writeList(averageRatingPerDiet);
    }

    public int describeContents() {
        return 0;
    }
}
