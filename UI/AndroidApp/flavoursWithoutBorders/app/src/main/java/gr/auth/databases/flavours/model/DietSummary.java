package gr.auth.databases.flavours.model;

public class DietSummary {
    private int id;
    private String name;

    public DietSummary(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
