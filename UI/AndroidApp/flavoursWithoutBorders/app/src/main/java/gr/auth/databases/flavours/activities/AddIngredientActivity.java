package gr.auth.databases.flavours.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.base.BaseActivity;
import gr.auth.databases.flavours.model.Ingredient;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;

import static gr.auth.databases.flavours.session.SessionManager.addIngredientUrl;

public class AddIngredientActivity extends BaseActivity {
    public static final String INGREDIENT_ADD_RESULT = "INGREDIENT_ADD_RESULT";

    private EditText ingredientNameInput;
    private RadioGroup ingredientHasAlcoholInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ingredient);

        Toolbar toolbar = findViewById(R.id.add_ingredient_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        }

        ingredientNameInput = findViewById(R.id.add_ingredient_name);
        ingredientHasAlcoholInput = findViewById(R.id.add_ingredient_contains_alcohol);

        AppCompatButton addIngredientButton = findViewById(R.id.add_ingredient_add_btn);
        addIngredientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int hasAlcoholResult = -1;
                switch (ingredientHasAlcoholInput.getCheckedRadioButtonId()) {
                    case R.id.add_ingredient_has_alcohol_yes:
                        hasAlcoholResult = 1;
                        break;
                    case R.id.add_ingredient_has_alcohol_no:
                        hasAlcoholResult = 0;
                        break;
                }

                if (hasAlcoholResult == -1) {
                    Toast.makeText(view.getContext(), "Does this ingredient have alcogol?", Toast.LENGTH_SHORT).show();
                    return;
                }

                AddIngredientTask addIngredientTask = new AddIngredientTask();
                addIngredientTask.execute();

                Intent returnIntent = new Intent();
                returnIntent.putExtra(INGREDIENT_ADD_RESULT, new Ingredient(ingredientNameInput.getText().toString(),
                        hasAlcoholResult == 1));
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

        createDrawer();
        drawer.setSelection(-1);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            drawer.openDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class AddIngredientTask extends AsyncTask<Void, Void, Integer> {
        private static final String REQ_INGREDIENT_NAME = "ingredient_name";
        private static final String REQ_INGREDIENT_HAS_ALCOHOL = "ingredient_has_alcohol";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Void... params) {
            int hasAlcoholResult = -1;
            switch (ingredientHasAlcoholInput.getCheckedRadioButtonId()) {
                case R.id.add_ingredient_has_alcohol_yes:
                    hasAlcoholResult = 1;
                    break;
                case R.id.add_ingredient_has_alcohol_no:
                    hasAlcoholResult = 0;
                    break;
            }

            //Builds the request
            RequestBody formBody = new FormBody.Builder()
                    .add(REQ_INGREDIENT_NAME, ingredientNameInput.getText().toString())
                    .add(REQ_INGREDIENT_HAS_ALCOHOL, hasAlcoholResult == 1 ? "True" : "False")
                    .build();
            Request request = new Request.Builder()
                    .url(addIngredientUrl)
                    .post(formBody)
                    .build();

            try {
                client.newCall(request).execute();
                return 0;
            } catch (Exception e) {
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            Toast.makeText(AddIngredientActivity.this,
                    "Ingredient was added!", Toast.LENGTH_SHORT).show();
        }
    }
}