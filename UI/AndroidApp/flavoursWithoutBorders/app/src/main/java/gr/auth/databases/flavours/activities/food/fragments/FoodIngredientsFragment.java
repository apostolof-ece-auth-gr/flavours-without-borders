package gr.auth.databases.flavours.activities.food.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.model.Ingredient;
import gr.auth.databases.flavours.utils.IngredientItemAdapter;

public class FoodIngredientsFragment extends Fragment {

    public FoodIngredientsFragment() {
        // Required empty public constructor
    }

    private static final String FOOD_INGREDIENTS = "FOOD_INGREDIENTS";

    private ArrayList<Ingredient> ingredients;

    public static FoodIngredientsFragment newInstance(ArrayList<Ingredient> ingredients) {
        FoodIngredientsFragment fragment = new FoodIngredientsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(FOOD_INGREDIENTS, ingredients);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        ingredients = getArguments().getParcelableArrayList(FOOD_INGREDIENTS);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.reusable_recycler_list, container, false);

        Context context = getContext();
        assert context != null;
        IngredientItemAdapter itemAdapter = new IngredientItemAdapter(context, ingredients);
        RecyclerView mainContent = rootView.findViewById(R.id.recycler_list);
        mainContent.setAdapter(itemAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mainContent.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mainContent.getContext(),
                layoutManager.getOrientation());
        mainContent.addItemDecoration(dividerItemDecoration);

        return rootView;
    }
}
