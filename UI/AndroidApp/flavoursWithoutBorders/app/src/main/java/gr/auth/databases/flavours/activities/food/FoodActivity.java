package gr.auth.databases.flavours.activities.food;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.activities.RateItemActivity;
import gr.auth.databases.flavours.activities.food.fragments.FoodInfoFragment;
import gr.auth.databases.flavours.activities.food.fragments.FoodIngredientsFragment;
import gr.auth.databases.flavours.activities.food.fragments.FoodRatingsFragment;
import gr.auth.databases.flavours.activities.ingredients.IngredientsActivity;
import gr.auth.databases.flavours.base.BaseActivity;
import gr.auth.databases.flavours.model.Food;
import gr.auth.databases.flavours.model.Ingredient;
import gr.auth.databases.flavours.model.ItemRating;
import gr.auth.databases.flavours.model.ItemSummary;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static gr.auth.databases.flavours.activities.RateItemActivity.BUNDLE_RATE_ITEM;
import static gr.auth.databases.flavours.activities.ingredients.IngredientsActivity.INGREDIENT_PICK_RESULT;
import static gr.auth.databases.flavours.session.SessionManager.addIngredientToFoodUrl;
import static gr.auth.databases.flavours.session.SessionManager.foodsUserViewUrl;

public class FoodActivity extends BaseActivity {
    public static final String BUNDLE_ARG_FOOD = "BUNDLE_ARG_FOOD";
    private static final int ADD_INGREDIENT_REQUEST = 420;

    private ItemSummary mFood;
    private Food food;
    private ArrayList<Ingredient> ingredients = new ArrayList<>();
    private ArrayList<ItemRating> ratings = new ArrayList<>();
    private ViewPager viewPager;
    private FloatingActionButton FAB;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);

        Bundle extras = getIntent().getExtras();
        assert extras != null;
        mFood = extras.getParcelable(BUNDLE_ARG_FOOD);

        Toolbar toolbar = findViewById(R.id.food_toolbar);
        toolbar.setTitle(mFood.getItemName());
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        }

        createDrawer();
        drawer.setSelection(-1);

        viewPager = findViewById(R.id.food_pager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                if (position == 0) {
                    FAB.hide();
                } else {
                    FAB.show();
                }
            }
        });

        FAB = findViewById(R.id.food_fab);
        FAB.hide();
        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewPager.getCurrentItem() == 1) {
                    Intent intent = new Intent(view.getContext(), IngredientsActivity.class);
                    startActivityForResult(intent, ADD_INGREDIENT_REQUEST);
                } else if (viewPager.getCurrentItem() == 2) {
                    Intent intent = new Intent(view.getContext(), RateItemActivity.class);
                    intent.putExtra(BUNDLE_RATE_ITEM, mFood);
                    startActivity(intent);
                }
            }
        });

        tabLayout = findViewById(R.id.food_tabs);

        FoodTask foodTask = new FoodTask();
        foodTask.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            drawer.openDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_INGREDIENT_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Ingredient result = data.getParcelableExtra(INGREDIENT_PICK_RESULT);
                AddIngredientToFood addIngredientToFood = new AddIngredientToFood();
                addIngredientToFood.execute(result);
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        RestaurantPagerAdapter adapter = new RestaurantPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(FoodInfoFragment.newInstance(food), "INFO");
        adapter.addFrag(FoodIngredientsFragment.newInstance(ingredients), "INGREDIENTS");
        adapter.addFrag(FoodRatingsFragment.newInstance(ratings), "RATINGS");
        viewPager.setAdapter(adapter);
    }

    private class RestaurantPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        RestaurantPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private class FoodTask extends AsyncTask<Void, Void, Integer> {
        private static final String JSON_TAG_FOOD_INFO = "foodInfo";
        private static final String JSON_TAG_FOOD_ID = "food_id";
        private static final String JSON_TAG_FOOD_NAME = "food_name";
        private static final String JSON_TAG_FOOD_DESCRIPTION = "food_description";
        private static final String JSON_TAG_FOOD_CALORIES = "food_calories";
        private static final String JSON_TAG_FOOD_AVG_RATING_OBJ = "averageRating";
        private static final String JSON_TAG_FOOD_AVG_RATING = "rating_grade__avg";

        private static final String JSON_TAG_FOOD_RESTAURANT_NAME = "restaurantName";

        private static final String JSON_TAG_FOOD_INGREDIENTS = "ingredients";
        private static final String JSON_TAG_FOOD_INGREDIENT_NAME = "ingredient_name";
        private static final String JSON_TAG_FOOD_INGREDIENT_HAS_ALCOHOL = "ingredient_has_alcohol";

        private static final String JSON_TAG_FOOD_RATINGS = "ratings";
        private static final String JSON_TAG_FOOD_RATING_GRADE = "rating_grade";
        private static final String JSON_TAG_FOOD_RATING_USER_ID = "user_id";
        private static final String JSON_TAG_FOOD_RATING_USERNAME = "username";
        private static final String JSON_TAG_FOOD_RATING_TEXT = "rating_text";
        private static final String JSON_TAG_FOOD_RATING_DATE = "rating_date";
        private static final String JSON_TAG_FOOD_RATING_PORTION_SIZE = "rating_pοrtion_size";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Void... params) {
            String requestUrl = foodsUserViewUrl + mFood.getId() + "/";

            //Builds the request
            Request request = new Request.Builder()
                    .url(requestUrl)
                    .build();

            try {
                //Makes request & handles response
                Response response = client.newCall(request).execute();

                ResponseBody responseBody = response.body();
                assert responseBody != null;
                String result = responseBody.string();
                JSONObject jsonResponse = new JSONObject(result);

                JSONObject jsonFoodInfo = jsonResponse.getJSONObject(JSON_TAG_FOOD_INFO);

                double avgRestaurantRating = -1;
                if (!jsonResponse.getJSONObject(JSON_TAG_FOOD_AVG_RATING_OBJ).isNull(JSON_TAG_FOOD_AVG_RATING)) {
                    avgRestaurantRating = jsonResponse.getJSONObject(JSON_TAG_FOOD_AVG_RATING_OBJ).getDouble(JSON_TAG_FOOD_AVG_RATING);
                }

                String foodCaloriesString = jsonFoodInfo.getString(JSON_TAG_FOOD_CALORIES);

                food = new Food(jsonFoodInfo.getInt(JSON_TAG_FOOD_ID),
                        jsonFoodInfo.getString(JSON_TAG_FOOD_NAME),
                        jsonResponse.getString(JSON_TAG_FOOD_RESTAURANT_NAME),
                        jsonFoodInfo.getString(JSON_TAG_FOOD_DESCRIPTION),
                        foodCaloriesString.equals("null") ? -1 : Integer.parseInt(foodCaloriesString),
                        avgRestaurantRating);

                JSONArray jsonIngredients = jsonResponse.getJSONArray(JSON_TAG_FOOD_INGREDIENTS);
                for (int ingredientIndex = 0; ingredientIndex < jsonIngredients.length(); ++ingredientIndex) {
                    JSONObject ingredient = jsonIngredients.getJSONObject(ingredientIndex);
                    ingredients.add(new Ingredient(ingredient.getString(JSON_TAG_FOOD_INGREDIENT_NAME),
                            ingredient.getBoolean(JSON_TAG_FOOD_INGREDIENT_HAS_ALCOHOL)));
                }

                JSONArray jsonRatings = jsonResponse.getJSONArray(JSON_TAG_FOOD_RATINGS);
                for (int ratingIndex = 0; ratingIndex < jsonRatings.length(); ++ratingIndex) {
                    JSONObject rating = jsonRatings.getJSONObject(ratingIndex);
                    ratings.add(new ItemRating(rating.getInt(JSON_TAG_FOOD_RATING_GRADE),
                            rating.getInt(JSON_TAG_FOOD_RATING_USER_ID),
                            rating.getString(JSON_TAG_FOOD_RATING_USERNAME),
                            rating.getString(JSON_TAG_FOOD_RATING_TEXT),
                            rating.getString(JSON_TAG_FOOD_RATING_DATE),
                            ItemRating.PortionSize.getEnumTypeFromString(
                                    rating.getString(JSON_TAG_FOOD_RATING_PORTION_SIZE))));
                }

                return 1;
            } catch (Exception e) {
                e.printStackTrace();
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            setupViewPager(viewPager);
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    private class AddIngredientToFood extends AsyncTask<Ingredient, Void, Integer> {
        private static final String JSON_TAG_FOOD_ID = "food";
        private static final String JSON_TAG_FOOD_INGREDIENT_NAME = "ingredient_name";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Integer doInBackground(Ingredient... params) {
            //Builds the request
            RequestBody formBody = new FormBody.Builder()
                    .add(JSON_TAG_FOOD_ID, "" + mFood.getId())
                    .add(JSON_TAG_FOOD_INGREDIENT_NAME, params[0].getName())
                    .build();
            Request request = new Request.Builder()
                    .url(addIngredientToFoodUrl)
                    .post(formBody)
                    .build();

            try {
                //Makes request & handles response
                client.newCall(request).execute();
                return 0;
            } catch (Exception e) {
                e.printStackTrace();
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            Toast.makeText(FoodActivity.this, "Ingredient added.", Toast.LENGTH_LONG).show();
        }
    }
}
