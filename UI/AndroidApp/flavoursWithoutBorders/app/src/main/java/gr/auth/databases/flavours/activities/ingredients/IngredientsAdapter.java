package gr.auth.databases.flavours.activities.ingredients;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import gr.auth.databases.flavours.R;
import gr.auth.databases.flavours.model.Ingredient;

public class IngredientsAdapter extends RecyclerView.Adapter<IngredientsAdapter.IngredientViewHolder> {
    private Context context;
    private ArrayList<Ingredient> ingredients;
    private ArrayList<String> userProhibitsIngredients;
    private SubscribeIngredientsAdapterInteractionListener subscribeIngredientsAdapterInteractionListener;
    private IngredientsAdapterInteractionListener ingredientsAdapterInteractionListener;

    IngredientsAdapter(Context context, ArrayList<Ingredient> ingredients, ArrayList<String> userProhibitsIngredients,
                       SubscribeIngredientsAdapterInteractionListener subscribeIngredientsAdapterInteractionListener,
                       IngredientsAdapterInteractionListener ingredientsAdapterInteractionListener) {
        this.context = context;
        this.ingredients = ingredients;
        this.userProhibitsIngredients = userProhibitsIngredients;
        this.subscribeIngredientsAdapterInteractionListener = subscribeIngredientsAdapterInteractionListener;
        this.ingredientsAdapterInteractionListener = ingredientsAdapterInteractionListener;
    }

    @NonNull
    @Override
    public IngredientsAdapter.IngredientViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                                      int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ingredients_row, parent, false);
        return new IngredientViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final IngredientViewHolder holder, final int position) {
        holder.name.setText(ingredients.get(position).getName());
        holder.containsAlcohol.setText(context.getString(R.string.ingredients_contains_alcohol_placeholder,
                ingredients.get(position).hasAlcohol() ? "YES" : "NO"));

        if (subscribeIngredientsAdapterInteractionListener != null) {
            holder.subscribeButton.setVisibility(View.VISIBLE);

            if (userProhibitsIngredients.contains(ingredients.get(position).getName())) {
                holder.subscribeButton.setImageResource(R.drawable.ic_delete_black_18dp);
            } else {
                holder.subscribeButton.setImageResource(R.drawable.ic_add_black_18dp);
            }

            holder.subscribeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subscribeIngredientsAdapterInteractionListener.onSubscribeIngredientsAdapterInteraction(
                            ingredients.get(holder.getAdapterPosition()));

                    if (userProhibitsIngredients.contains(ingredients.get(position).getName())) {
                        holder.subscribeButton.setImageResource(R.drawable.ic_add_black_18dp);
                        userProhibitsIngredients.remove(ingredients.get(position).getName());
                    } else {
                        holder.subscribeButton.setImageResource(R.drawable.ic_delete_black_18dp);
                        userProhibitsIngredients.add(ingredients.get(position).getName());
                    }
                }
            });
        } else {
            holder.subscribeButton.setVisibility(View.GONE);
        }

        if (ingredientsAdapterInteractionListener != null) {
            holder.card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ingredientsAdapterInteractionListener.onIngredientsAdapterInteraction(
                            ingredients.get(holder.getAdapterPosition()));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return ingredients.size();
    }

    static class IngredientViewHolder extends RecyclerView.ViewHolder {
        CardView card;
        TextView name, containsAlcohol;
        AppCompatImageButton subscribeButton;

        IngredientViewHolder(View v) {
            super(v);
            card = v.findViewById(R.id.ingredients_row_card);
            name = v.findViewById(R.id.ingredients_ingredient_name);
            containsAlcohol = v.findViewById(R.id.ingredients_ingredient_contains_alcohol);
            subscribeButton = v.findViewById(R.id.ingredients_ingredient_subscribe);
        }
    }

    public interface SubscribeIngredientsAdapterInteractionListener {
        void onSubscribeIngredientsAdapterInteraction(Ingredient ingredient);
    }

    public interface IngredientsAdapterInteractionListener {
        void onIngredientsAdapterInteraction(Ingredient ingredient);
    }
}