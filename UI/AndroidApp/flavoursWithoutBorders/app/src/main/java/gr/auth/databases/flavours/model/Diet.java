package gr.auth.databases.flavours.model;

public class Diet {
    private int id;
    private String name, description;
    private boolean isFollowedByUser, isAccepted;

    public Diet(int id, String name, String description, boolean isFollowedByUser, boolean isAccepted) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.isFollowedByUser = isFollowedByUser;
        this.isAccepted = isAccepted;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isFollowedByUser() {
        return isFollowedByUser;
    }

    public void setFollowedByUser(boolean followedByUser) {
        isFollowedByUser = followedByUser;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean accepted) {
        isAccepted = accepted;
    }
}
