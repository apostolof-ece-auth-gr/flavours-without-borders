package gr.auth.databases.flavours.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Rating implements Parcelable {
    private int grade, userID;
    private String username, text, date;

    Rating(int grade, int userID, String username, String text, String date) {
        this.grade = grade;
        this.userID = userID;
        this.username = username;
        this.text = text;
        this.date = date;
    }

    public int getGrade() {
        return grade;
    }

    public int getUserID() {
        return userID;
    }

    public String getUsername() {
        return username;
    }

    public String getText() {
        return text;
    }

    public String getDate() {
        return date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(grade);
        out.writeInt(userID);
        out.writeString(username);
        out.writeString(text);
        out.writeString(date);
    }

    public static final Parcelable.Creator<Rating> CREATOR = new Parcelable.Creator<Rating>() {
        public Rating createFromParcel(Parcel in) {
            return new Rating(in);
        }

        public Rating[] newArray(int size) {
            return new Rating[size];
        }
    };

    Rating(Parcel in) {
        grade = in.readInt();
        userID = in.readInt();
        username = in.readString();
        text = in.readString();
        date = in.readString();
    }
}
