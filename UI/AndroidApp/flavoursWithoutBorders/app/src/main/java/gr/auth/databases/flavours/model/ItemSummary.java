package gr.auth.databases.flavours.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

public class ItemSummary implements Parcelable {
    public enum ItemType {
        FOOD(0), DRINK(1), UNSUPPORTED(-1);

        private int id;

        ItemType(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        @Nullable
        public static String getWorkingTypeFromId(int id) {
            switch (id) {
                case 0:
                    return "Food";
                case 1:
                    return "Drink";
                default:
                    return null;
            }
        }

        public static ItemType getEnumTypeFromId(int id) {
            switch (id) {
                case 0:
                    return FOOD;
                case 1:
                    return DRINK;
                default:
                    return UNSUPPORTED;
            }
        }
    }

    private String itemName;
    private int id;
    private ItemType type;
    private boolean isAccepted;

    public ItemSummary(String item, int id, ItemType type, boolean isAccepted) {
        this.itemName = item;
        this.id = id;
        this.type = type;
        this.isAccepted = isAccepted;
    }

    public String getItemName() {
        return itemName;
    }

    public int getId() {
        return id;
    }

    public ItemType getType() {
        return type;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean accepted) {
        isAccepted = accepted;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(itemName);
        out.writeInt(type.getId());
        out.writeByte((byte) (isAccepted ? 1 : 0));
    }

    public static final Parcelable.Creator<ItemSummary> CREATOR = new Parcelable.Creator<ItemSummary>() {
        public ItemSummary createFromParcel(Parcel in) {
            return new ItemSummary(in);
        }

        public ItemSummary[] newArray(int size) {
            return new ItemSummary[size];
        }
    };

    private ItemSummary(Parcel in) {
        id = in.readInt();
        itemName = in.readString();
        type = ItemType.getEnumTypeFromId(in.readInt());
        isAccepted = in.readByte() != 0;
    }
}
