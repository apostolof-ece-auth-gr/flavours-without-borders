CREATE DATABASE  IF NOT EXISTS `flavours_without_borders` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `flavours_without_borders`;
-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 83.212.109.171    Database: flavours_without_borders
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `diet`
--

DROP TABLE IF EXISTS `diet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diet` (
  `diet_id` int(11) NOT NULL,
  `diet_name` varchar(500) NOT NULL,
  `diet_description` varchar(700) DEFAULT NULL,
  `diet_is_approved` bit(1) DEFAULT b'0',
  PRIMARY KEY (`diet_id`),
  UNIQUE KEY `diet_id_UNIQUE` (`diet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diet`
--

LOCK TABLES `diet` WRITE;
/*!40000 ALTER TABLE `diet` DISABLE KEYS */;
INSERT INTO `diet` VALUES (2,'Διαβητικός/Διαβητική','Χαρακτηρίζεται από φαγητά χαμηλά σε ζάχαρη',_binary ''),(5,'Fruitarian','Διατροφή που αποτελείται κυρίως από φρούτα',_binary ''),(7,'Vegan','Not found',_binary '\0'),(9,'Ketogenic','Πρέπει να πετύχεις τη λεγόμενη κέτωση',_binary '\0'),(13,'Bulking','Μόνο κρέας',_binary ''),(14,'Kangatarian','Μορφή vegeterian διατροφής που περιλαμβάνει όμως κρέας κανγουρό',_binary ''),(18,'Christian Fasting','Όχι τροφές παραγώμενες από ζώα και όχι λάδι',_binary ''),(20,'Vegetarian','Όχι vegan',_binary '\0');
/*!40000 ALTER TABLE `diet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diet_prohibits_ingredient`
--

DROP TABLE IF EXISTS `diet_prohibits_ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diet_prohibits_ingredient` (
  `diet_id` int(11) NOT NULL,
  `ingredient_name` varchar(500) NOT NULL,
  PRIMARY KEY (`diet_id`,`ingredient_name`),
  KEY `diet_prohibits_ingredient_name_idx` (`ingredient_name`),
  CONSTRAINT `diet_prohibits_has_diet_id` FOREIGN KEY (`diet_id`) REFERENCES `diet` (`diet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `diet_prohibits_ingredient_name` FOREIGN KEY (`ingredient_name`) REFERENCES `ingredient` (`ingredient_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diet_prohibits_ingredient`
--

LOCK TABLES `diet_prohibits_ingredient` WRITE;
/*!40000 ALTER TABLE `diet_prohibits_ingredient` DISABLE KEYS */;
INSERT INTO `diet_prohibits_ingredient` VALUES (2,'sugar'),(13,'whiskey'),(5,'κιμάς'),(5,'μοσχαρίσια πόδια'),(18,'μοσχαρίσια πόδια'),(5,'πατάτα'),(5,'πιπεριά'),(5,'ρύζι'),(5,'ψάρι'),(18,'ψάρι');
/*!40000 ALTER TABLE `diet_prohibits_ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drink`
--

DROP TABLE IF EXISTS `drink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drink` (
  `drink_id` int(11) NOT NULL,
  `drink_name` varchar(500) NOT NULL,
  `drink_description` varchar(700) DEFAULT NULL,
  `drink_has_alcohol` bit(1) NOT NULL,
  `drink_is_approved` bit(1) DEFAULT b'0',
  `restaurant_id` int(11) NOT NULL,
  PRIMARY KEY (`drink_id`),
  UNIQUE KEY `drink_id_UNIQUE` (`drink_id`),
  KEY `food_is_offered_by_restaurant_id_idx` (`restaurant_id`),
  CONSTRAINT `drink_is_offered_by_restaurant_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`restaurant_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drink`
--

LOCK TABLES `drink` WRITE;
/*!40000 ALTER TABLE `drink` DISABLE KEYS */;
INSERT INTO `drink` VALUES (3,'Κόκκινη πλατεία','Απολαυστικό τσάι με ιβίσκο',_binary '\0',_binary '',768),(25,'Λεμονάδα',NULL,_binary '\0',_binary '\0',4330),(46,'Long Island',NULL,_binary '',_binary '',768),(69,'Πορτοκαλάδα',NULL,_binary '\0',_binary '',9),(494,'La Trappe Quadrupel','Μερικοί υποστηρίζουν ότι είναι η καλύτερη μοναστηριακή μπύρα στον κόσμο',_binary '',_binary '',683),(984,'Long Island','Το πιο βαρή coctail που υπάρχει',_binary '',_binary '',1356),(3214,'Mojito','Δροσιστικό κουβανέζικο κοκτέιλ',_binary '',_binary '',1356),(3568,'Τσάι του βουνού','Καλό για τα λαιμά',_binary '\0',_binary '\0',236),(5673,'Ρετσίνα Μαλαματίνα','ΜΠΑΟΚ ΡΕ',_binary '',_binary '\0',8132),(6363,'White Russian','Διάσημο coctail που έπινε ο Jeff Bridges στη ταινία \"The big Lebowski\"',_binary '',_binary '',6211),(26427,'Old Fashioned','Κοκτέιλ με αμερικάνικες ρίζες',_binary '',_binary '',6211);
/*!40000 ALTER TABLE `drink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drink_has_ingredient`
--

DROP TABLE IF EXISTS `drink_has_ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drink_has_ingredient` (
  `drink_id` int(11) NOT NULL,
  `ingredient_name` varchar(500) NOT NULL,
  PRIMARY KEY (`drink_id`,`ingredient_name`),
  KEY `ingredient_name_idx` (`ingredient_name`),
  CONSTRAINT `drink_has_drink_id` FOREIGN KEY (`drink_id`) REFERENCES `drink` (`drink_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `drink_has_ingredient_name` FOREIGN KEY (`ingredient_name`) REFERENCES `ingredient` (`ingredient_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drink_has_ingredient`
--

LOCK TABLES `drink_has_ingredient` WRITE;
/*!40000 ALTER TABLE `drink_has_ingredient` DISABLE KEYS */;
INSERT INTO `drink_has_ingredient` VALUES (26427,'Angostura bitters'),(3214,'club soda'),(6363,'coffee liqueur'),(46,'Cola'),(984,'Cola'),(6363,'cream'),(46,'Gin'),(984,'Gin'),(984,'Lemon juice'),(46,'Lemon slice'),(3214,'lime'),(3214,'mint'),(3214,'rum'),(46,'Silver tequila'),(984,'Silver tequila'),(3214,'sugar'),(26427,'sugar'),(46,'Sweet and sour mix'),(46,'Triple sec'),(984,'Triple sec'),(6363,'vodka'),(26427,'whiskey'),(46,'White rum'),(984,'White rum');
/*!40000 ALTER TABLE `drink_has_ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food`
--

DROP TABLE IF EXISTS `food`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food` (
  `food_id` int(11) NOT NULL,
  `food_name` varchar(500) NOT NULL,
  `food_description` varchar(700) DEFAULT NULL,
  `food_calories` int(11) DEFAULT NULL,
  `food_is_approved` bit(1) DEFAULT b'0',
  `restaurant_id` int(11) NOT NULL,
  PRIMARY KEY (`food_id`),
  UNIQUE KEY `food_id_UNIQUE` (`food_id`),
  KEY `food_is_offered_by_restaurant_id_idx` (`restaurant_id`),
  CONSTRAINT `food_is_offered_by_restaurant_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`restaurant_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food`
--

LOCK TABLES `food` WRITE;
/*!40000 ALTER TABLE `food` DISABLE KEYS */;
INSERT INTO `food` VALUES (2,'Κεφτεδάκια με πατάτες',NULL,450,_binary '',8132),(4,'Κοτομπουκιές',NULL,NULL,_binary '\0',4330),(6,'Γεμιστά','Λαχταριστές ντομάτες και πιπεριές γεμιστές',326,_binary '',8),(8,'Κεφτεδάκια με πατάτες',NULL,NULL,_binary '\0',8),(48,'Burger','Είναι σαν να τρως αέρα!',1821,_binary '',15),(68,'Μπριζολάκια','Χοιρινές μπριζόλες ξεροψημένες!',NULL,_binary '\0',8132),(73,'Τυροκαυτερή','Η καλύτερη συνοδεία για τα τηγανητά κολοκυθάκια',276,_binary '',8132),(124,'Γεμιστά','Παραδοσιακό ελληνικό πιάτο',392,_binary '',4330),(478,'Πατσάς με σκορδοστούμπι','Ένα πιάτο που θυμίζει πολύ χωριό',450,_binary '',4330),(548,'Fish&Chips','Μία γεύση από Αγγλία',840,_binary '',7365);
/*!40000 ALTER TABLE `food` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_has_ingredient`
--

DROP TABLE IF EXISTS `food_has_ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_has_ingredient` (
  `food_id` int(11) NOT NULL,
  `ingredient_name` varchar(500) NOT NULL,
  PRIMARY KEY (`food_id`,`ingredient_name`),
  KEY `ingredient_name_idx` (`ingredient_name`),
  CONSTRAINT `food_has_food_id` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `food_has_ingredient_name` FOREIGN KEY (`ingredient_name`) REFERENCES `ingredient` (`ingredient_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_has_ingredient`
--

LOCK TABLES `food_has_ingredient` WRITE;
/*!40000 ALTER TABLE `food_has_ingredient` DISABLE KEYS */;
INSERT INTO `food_has_ingredient` VALUES (2,'αλάτι'),(6,'αλάτι'),(8,'αλάτι'),(124,'αλάτι'),(548,'αλάτι'),(2,'κιμάς'),(8,'κιμάς'),(124,'κιμάς'),(6,'κουκουνάρι'),(2,'κρεμμύδι'),(124,'κρεμμύδι'),(2,'λάδι'),(6,'λάδι'),(8,'λάδι'),(124,'λάδι'),(548,'λάδι'),(8,'μαϊντανός'),(124,'μαϊντανός'),(478,'μοσχαρίσια πόδια'),(478,'μπούκοβο'),(6,'ντομάτα'),(8,'ντομάτα'),(124,'ντομάτα'),(2,'πατάτα'),(8,'πατάτα'),(548,'πατάτα'),(6,'πιπεριά'),(124,'πιπεριά'),(6,'ρύζι'),(124,'ρύζι'),(478,'σκόρδο'),(548,'ψάρι');
/*!40000 ALTER TABLE `food_has_ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredient`
--

DROP TABLE IF EXISTS `ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredient` (
  `ingredient_name` varchar(500) NOT NULL,
  `ingredient_has_alcohol` bit(1) DEFAULT b'0',
  PRIMARY KEY (`ingredient_name`),
  UNIQUE KEY `ingredient_name_UNIQUE` (`ingredient_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient`
--

LOCK TABLES `ingredient` WRITE;
/*!40000 ALTER TABLE `ingredient` DISABLE KEYS */;
INSERT INTO `ingredient` VALUES ('Angostura bitters',_binary ''),('club soda',_binary '\0'),('coffee liqueur',_binary '\0'),('Cola',_binary '\0'),('cream',_binary '\0'),('Gin',_binary ''),('Lemon juice',_binary '\0'),('Lemon slice',_binary '\0'),('lime',_binary '\0'),('mint',_binary '\0'),('rum',_binary ''),('Silver tequila',_binary ''),('sugar',_binary '\0'),('Sweet and sour mix',_binary '\0'),('Triple sec',_binary ''),('vodka',_binary ''),('whiskey',_binary ''),('White rum',_binary ''),('αλάτι',_binary '\0'),('κιμάς',_binary '\0'),('κουκουνάρι',_binary '\0'),('κρεμμύδι',_binary '\0'),('λάδι',_binary '\0'),('μαϊντανός',_binary '\0'),('μοσχαρίσια πόδια',_binary '\0'),('μπούκοβο',_binary '\0'),('ντομάτα',_binary '\0'),('πατάτα',_binary '\0'),('πιπεριά',_binary '\0'),('ρύζι',_binary '\0'),('σκόρδο',_binary '\0'),('ψάρι',_binary '\0');
/*!40000 ALTER TABLE `ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `mods_view`
--

DROP TABLE IF EXISTS `mods_view`;
/*!50001 DROP VIEW IF EXISTS `mods_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `mods_view` AS SELECT 
 1 AS `restaurant_id`,
 1 AS `restaurant_name`,
 1 AS `restaurant_category`,
 1 AS `restaurant_longitude`,
 1 AS `restaurant_latitude`,
 1 AS `restaurant_opening`,
 1 AS `restaurant_closing`,
 1 AS `user_id`,
 1 AS `food_id`,
 1 AS `food_name`,
 1 AS `food_description`,
 1 AS `food_calories`,
 1 AS `drink_id`,
 1 AS `drink_name`,
 1 AS `drink_description`,
 1 AS `ingredient_name`,
 1 AS `ingredient_has_alcohol`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `permission_id` int(11) NOT NULL,
  `permission_description` varchar(700) NOT NULL,
  PRIMARY KEY (`permission_id`),
  UNIQUE KEY `permission_id_UNIQUE` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` VALUES (2,'Μπορεί να προτείνει νέα δίαιτα'),(4,'Μπορεί να προσθέσει πιάτο χωρίς έγκριση'),(8,'Μπορεί να εγκρίνει κατάστημα'),(16,'Μπορεί να αξιολογήσει καταστήματα'),(32,'Μπορεί να αξιολογήσει ποτό'),(64,'Μπορεί να κάνει ban χρήστη');
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant`
--

DROP TABLE IF EXISTS `restaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant` (
  `restaurant_id` int(11) NOT NULL,
  `restaurant_name` varchar(500) NOT NULL,
  `restaurant_category` enum('cafeteria','pub','bar','restaurant','fast_food','ethnic') NOT NULL,
  `restaurant_longitude` float NOT NULL,
  `restaurant_latitude` float NOT NULL,
  `restaurant_opening` time DEFAULT NULL,
  `restaurant_closing` time DEFAULT NULL,
  `restaurant_is_approved` bit(1) DEFAULT b'0' COMMENT 'Ίσως πρέπει να γίνει NN και να φύγει το default;',
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`restaurant_id`),
  UNIQUE KEY `restaurant_id_UNIQUE` (`restaurant_id`),
  KEY `restaurant_is_owned_by_user_id_idx` (`user_id`),
  CONSTRAINT `restaurant_is_owned_by_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant`
--

LOCK TABLES `restaurant` WRITE;
/*!40000 ALTER TABLE `restaurant` DISABLE KEYS */;
INSERT INTO `restaurant` VALUES (7,'U Fleků','pub',40.6357,22.9367,'18:00:00','04:00:00',_binary '\0',NULL),(8,'Σ σύγχρονα εστιατόρια','restaurant',40.6173,22.9597,'12:00:00','22:00:00',_binary '',NULL),(9,'U Fleků Garden','pub',40.6354,22.937,'18:00:00','02:00:00',_binary '',NULL),(15,'McDonald\'s','fast_food',40.6463,22.9206,'12:00:00','23:59:00',_binary '',12),(129,'Grill 15','fast_food',40.6215,22.961,'12:00:00','01:30:00',_binary '\0',NULL),(236,'Butterflies and Hurricanes','cafeteria',40.6147,22.9609,'08:00:00','20:00:00',_binary '',356),(683,'The Hoppy Pub','pub',40.6279,22.9489,'17:30:00','01:30:00',_binary '',NULL),(768,'Belleville sin patron','bar',40.6337,22.9518,'12:00:00','02:00:00',_binary '',7),(1356,'Αιθερόπλοο','pub',40.6144,22.9598,'08:00:00','02:00:00',_binary '',356),(4330,'Οδυσσέας','restaurant',40.6369,22.9534,'12:30:00','18:00:00',_binary '',24788),(6211,'Pulp','bar',40.6321,22.9479,'09:00:00','23:59:00',_binary '',NULL),(7365,'McDonald\'s','fast_food',40.5745,22.9715,'12:00:00','23:59:00',_binary '',NULL),(8132,'Γιαννούλα','restaurant',40.64,22.9468,'13:30:00','01:00:00',_binary '',1566);
/*!40000 ALTER TABLE `restaurant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(500) NOT NULL,
  `role_description` varchar(700) NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_id_UNIQUE` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (0,'user','Απλός χρήστης'),(1,'moderator','Διαχειριστής'),(2,'owner','Ιδιοκτήτης');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permission`
--

DROP TABLE IF EXISTS `role_has_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permission` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`),
  KEY `role_has_permission_id_idx` (`permission_id`),
  CONSTRAINT `role_has_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`permission_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `role_has_permission_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permission`
--

LOCK TABLES `role_has_permission` WRITE;
/*!40000 ALTER TABLE `role_has_permission` DISABLE KEYS */;
INSERT INTO `role_has_permission` VALUES (2,4),(1,8),(0,16),(0,32),(1,64);
/*!40000 ALTER TABLE `role_has_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `user_username` varchar(500) NOT NULL,
  `user_hashed_password` char(64) NOT NULL,
  `user_salt` char(32) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_age` date NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `user_is_role_id_idx` (`role_id`),
  CONSTRAINT `user_is_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (7,'gardenia@kyuss.com','Γαρδένιος ο Stoner','A8F08F6BE2B5F7280BFB0EF42850C7AF7DC9F9512655DF960F2CEE86A8734092','cd42e1cafe678f9473e05af4540a92cb',0,'1995-03-25'),(9,'waves@naxatras.gr','johny2012','A2D5D620335018723BCFFE6469CEABCC75B72C180BAD8637B220C43185D28B7A','0AB4E80109BF647D1BD2FA67833E3E30',1,'1983-09-15'),(12,'amail@yahoo.com','marika','97BB101F9ECF31FFD3788D77C1A02C431C1607E8168AAA53E38A89D445324428','e8413a21fbfef293ed80ec5ac0864ab9',0,'1976-10-12'),(215,'mongoose@fumanchu.com','Μαγκούστα','95BDEEACDAE5B9E371976D84387194DE83EB5F58AB7EAF4F4CD533925E9CDB3B','c7bcc1683f9a415188d9cb9bba8023df',0,'1978-11-02'),(356,'nonexistent@noidea.bn','Ανύπαρκτος','09986D06B9EABA0200318069CD372659C6036E2B9F0B1D44083F438E5DDE5821','vjp4a15w7n6806fmhe3x45qxz988xxql',2,'1992-04-14'),(666,'the_trooper@iron.maiden.uk','eddie lives inside you','B87814E1BBF9D5862653659D9EE77F9F9F44A3B88CBDEA4A56A97796F19CE817','f361b8aa332b67eb99c7bb5abcfb33b5',0,'1975-12-25'),(1566,'iamanonymous@not.your.buisness.ελ','Ανώνυμος','5F4783987A5E5B9B2D7FBA6BA5D8879A6EA94D24CE374570BD74CFF205D2E552','7g2pnmg118k9h4h5zphkntixwb4wtyn8',2,'1987-06-23'),(8756,'godzilla@fumanchu.com','Ms Godzila','E9295989293B86B016200A8FFB37F74443BF478BFCEC36266105E83C4455DFBF','5070b798b96b6038d75c0cbf8ae13cb8',1,'1994-08-20'),(24788,'somebody@gmail.com','κάποιος','7D148308053C85B038157848103489FCCFAA628044BC36427EEED94AD2B70E4F','05a700266be1bf1b84dd2c7c4a19f097',2,'1989-03-28'),(75813,'desert_cruiser@truckfighters.se','Νταλίκας','20AE282A6814EF1865E7EDD8D379281001F6E7AA4F8378DA26B6AB0187D61500','43f7cc8bec190e0c20fc2a13cd3dab86',0,'2001-12-24');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_follows_diet`
--

DROP TABLE IF EXISTS `user_follows_diet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_follows_diet` (
  `user_id` int(11) NOT NULL,
  `diet_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`diet_id`),
  KEY `user_follows_diet_id_idx` (`diet_id`),
  CONSTRAINT `user_follows_diet_id` FOREIGN KEY (`diet_id`) REFERENCES `diet` (`diet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_follows_has_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_follows_diet`
--

LOCK TABLES `user_follows_diet` WRITE;
/*!40000 ALTER TABLE `user_follows_diet` DISABLE KEYS */;
INSERT INTO `user_follows_diet` VALUES (1566,2),(9,5),(666,13),(75813,13),(1566,14),(9,18),(12,18),(1566,18);
/*!40000 ALTER TABLE `user_follows_diet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_prohibits_ingredient`
--

DROP TABLE IF EXISTS `user_prohibits_ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_prohibits_ingredient` (
  `user_id` int(11) NOT NULL,
  `ingredient_name` varchar(500) NOT NULL,
  PRIMARY KEY (`user_id`,`ingredient_name`),
  KEY `user_prohibits_ingredient_name_idx` (`ingredient_name`),
  CONSTRAINT `user_prohibits_has_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_prohibits_ingredient_name` FOREIGN KEY (`ingredient_name`) REFERENCES `ingredient` (`ingredient_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_prohibits_ingredient`
--

LOCK TABLES `user_prohibits_ingredient` WRITE;
/*!40000 ALTER TABLE `user_prohibits_ingredient` DISABLE KEYS */;
INSERT INTO `user_prohibits_ingredient` VALUES (356,'club soda'),(75813,'Cola'),(7,'μαϊντανός'),(7,'μπούκοβο'),(12,'μπούκοβο'),(9,'πιπεριά'),(7,'σκόρδο'),(12,'σκόρδο'),(8756,'σκόρδο');
/*!40000 ALTER TABLE `user_prohibits_ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_rates_drink`
--

DROP TABLE IF EXISTS `user_rates_drink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_rates_drink` (
  `user_id` int(11) NOT NULL,
  `drink_id` int(11) NOT NULL,
  `rating_grade` int(11) NOT NULL,
  `rating_date` date NOT NULL,
  `rating_text` varchar(700) DEFAULT NULL,
  `rating_pοrtion_size` enum('small','medium','big') DEFAULT NULL,
  PRIMARY KEY (`user_id`,`drink_id`),
  KEY `user_rates_drink_id_idx` (`drink_id`),
  CONSTRAINT `user_rates_drink_has_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_rates_drink_id` FOREIGN KEY (`drink_id`) REFERENCES `drink` (`drink_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_rates_drink`
--

LOCK TABLES `user_rates_drink` WRITE;
/*!40000 ALTER TABLE `user_rates_drink` DISABLE KEYS */;
INSERT INTO `user_rates_drink` VALUES (7,46,5,'2016-10-12','Πολύ σωστά φτιαγμένο!','big'),(7,984,4,'2017-12-06','Αρκετά καλό','medium'),(9,26427,2,'2018-06-28','Μετριότης κύριοι.','small'),(12,3214,4,'2018-12-17',NULL,'medium'),(12,6363,5,'2018-07-08','Τέλειο!!1',NULL),(215,3214,5,'2018-05-01',NULL,NULL),(666,494,5,'2015-06-11',NULL,NULL),(8756,494,5,'2017-05-10','ΜΠΥΡΑΡΑΑΑΑΑΑΑ!!!!!',NULL),(75813,3,3,'2018-11-04','Δεν ήταν φρέσκια',NULL),(75813,69,4,'2018-04-05',NULL,'medium');
/*!40000 ALTER TABLE `user_rates_drink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_rates_food`
--

DROP TABLE IF EXISTS `user_rates_food`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_rates_food` (
  `user_id` int(11) NOT NULL,
  `food_id` int(11) NOT NULL,
  `rating_grade` int(11) NOT NULL,
  `rating_date` date NOT NULL,
  `rating_text` varchar(700) DEFAULT NULL,
  `rating_pοrtion_size` enum('small','medium','big') DEFAULT NULL,
  PRIMARY KEY (`user_id`,`food_id`),
  KEY `user_rates_food_id_idx` (`food_id`),
  CONSTRAINT `user_rates_food_has_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_rates_food_id` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_rates_food`
--

LOCK TABLES `user_rates_food` WRITE;
/*!40000 ALTER TABLE `user_rates_food` DISABLE KEYS */;
INSERT INTO `user_rates_food` VALUES (7,6,4,'2018-08-12',NULL,'medium'),(9,6,4,'2018-12-22','Νόστιμα!','big'),(9,48,2,'2017-09-07','Δε χόρτασα καθόλου','medium'),(9,73,5,'2016-06-15','Πολύ καλή','big'),(12,2,5,'2016-07-17',NULL,NULL),(12,73,5,'2017-03-28',NULL,NULL),(215,548,1,'2018-09-07',NULL,'small'),(8756,124,3,'2018-11-11','Μέτριο',NULL),(8756,478,5,'2018-03-25','Πολύ γευστικό',NULL),(75813,124,2,'2018-08-20',NULL,'medium');
/*!40000 ALTER TABLE `user_rates_food` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_rates_restaurant`
--

DROP TABLE IF EXISTS `user_rates_restaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_rates_restaurant` (
  `user_id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `diet_id` int(11) DEFAULT NULL,
  `rating_grade` int(11) NOT NULL,
  `rating_date` date NOT NULL,
  `rating_text` varchar(700) DEFAULT NULL,
  `rating_accessibility` enum('easy','moderate','hard') DEFAULT NULL,
  PRIMARY KEY (`user_id`,`restaurant_id`),
  KEY `user_rates_restaurant_id_idx` (`restaurant_id`),
  KEY `user_rates_restaurant_with_diet_id_idx` (`diet_id`),
  CONSTRAINT `user_rates_restaurant_has_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_rates_restaurant_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`restaurant_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_rates_restaurant_with_diet_id` FOREIGN KEY (`diet_id`) REFERENCES `diet` (`diet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_rates_restaurant`
--

LOCK TABLES `user_rates_restaurant` WRITE;
/*!40000 ALTER TABLE `user_rates_restaurant` DISABLE KEYS */;
INSERT INTO `user_rates_restaurant` VALUES (7,15,2,2,'2017-06-01','Χάλι μαύρο',NULL),(7,7365,NULL,3,'2018-02-21','Μέτριο.',NULL),(9,15,NULL,3,'2016-05-08','Δεν χόρτασα','easy'),(215,4330,18,5,'2018-10-02','Χορταστικό φαΐ!','hard'),(215,6211,NULL,4,'2018-04-04','','hard'),(356,4330,NULL,4,'2018-09-16',NULL,NULL),(666,7365,NULL,3,'2018-04-16',NULL,'easy'),(8756,15,18,2,'2017-09-11',NULL,'moderate'),(24788,1356,NULL,5,'2018-06-08','Φιλικότατο προσωπικό!','moderate'),(24788,4330,2,3,'2018-01-04','Λίγες επιλογές για διαβητικούς.',NULL);
/*!40000 ALTER TABLE `user_rates_restaurant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `users_view`
--

DROP TABLE IF EXISTS `users_view`;
/*!50001 DROP VIEW IF EXISTS `users_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `users_view` AS SELECT 
 1 AS `restaurant_id`,
 1 AS `restaurant_name`,
 1 AS `restaurant_category`,
 1 AS `restaurant_longitude`,
 1 AS `restaurant_latitude`,
 1 AS `restaurant_opening`,
 1 AS `restaurant_closing`,
 1 AS `user_id`,
 1 AS `food_id`,
 1 AS `food_name`,
 1 AS `food_description`,
 1 AS `food_calories`,
 1 AS `drink_id`,
 1 AS `drink_name`,
 1 AS `drink_description`,
 1 AS `ingredient_name`,
 1 AS `ingredient_has_alcohol`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `mods_view`
--

/*!50001 DROP VIEW IF EXISTS `mods_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`flavoursUser`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `mods_view` AS select `restaurant`.`restaurant_id` AS `restaurant_id`,`restaurant`.`restaurant_name` AS `restaurant_name`,`restaurant`.`restaurant_category` AS `restaurant_category`,`restaurant`.`restaurant_longitude` AS `restaurant_longitude`,`restaurant`.`restaurant_latitude` AS `restaurant_latitude`,`restaurant`.`restaurant_opening` AS `restaurant_opening`,`restaurant`.`restaurant_closing` AS `restaurant_closing`,`restaurant`.`user_id` AS `user_id`,NULL AS `food_id`,NULL AS `food_name`,NULL AS `food_description`,NULL AS `food_calories`,NULL AS `drink_id`,NULL AS `drink_name`,NULL AS `drink_description`,NULL AS `ingredient_name`,NULL AS `ingredient_has_alcohol` from `restaurant` where (`restaurant`.`restaurant_is_approved` = FALSE) union select `restaurant`.`restaurant_id` AS `restaurant_id`,`restaurant`.`restaurant_name` AS `restaurant_name`,`restaurant`.`restaurant_category` AS `restaurant_category`,`restaurant`.`restaurant_longitude` AS `restaurant_longitude`,`restaurant`.`restaurant_latitude` AS `restaurant_latitude`,`restaurant`.`restaurant_opening` AS `restaurant_opening`,`restaurant`.`restaurant_closing` AS `restaurant_closing`,`restaurant`.`user_id` AS `user_id`,`food`.`food_id` AS `food_id`,`food`.`food_name` AS `food_name`,`food`.`food_description` AS `food_description`,`food`.`food_calories` AS `food_calories`,NULL AS `drink_id`,NULL AS `drink_name`,NULL AS `drink_description`,`ingredient`.`ingredient_name` AS `ingredient_name`,`ingredient`.`ingredient_has_alcohol` AS `ingredient_has_alcohol` from (((`restaurant` left join `food` on((`restaurant`.`restaurant_id` = `food`.`restaurant_id`))) left join `food_has_ingredient` on((`food`.`food_id` = `food_has_ingredient`.`food_id`))) left join `ingredient` on((`food_has_ingredient`.`ingredient_name` = `ingredient`.`ingredient_name`))) where (`food`.`food_is_approved` = FALSE) union select `restaurant`.`restaurant_id` AS `restaurant_id`,`restaurant`.`restaurant_name` AS `restaurant_name`,`restaurant`.`restaurant_category` AS `restaurant_category`,`restaurant`.`restaurant_longitude` AS `restaurant_longitude`,`restaurant`.`restaurant_latitude` AS `restaurant_latitude`,`restaurant`.`restaurant_opening` AS `restaurant_opening`,`restaurant`.`restaurant_closing` AS `restaurant_closing`,`restaurant`.`user_id` AS `user_id`,NULL AS `food_id`,NULL AS `food_name`,NULL AS `food_description`,NULL AS `food_calories`,`drink`.`drink_id` AS `drink_id`,`drink`.`drink_name` AS `drink_name`,`drink`.`drink_description` AS `drink_description`,`ingredient`.`ingredient_name` AS `ingredient_name`,`ingredient`.`ingredient_has_alcohol` AS `ingredient_has_alcohol` from (((`restaurant` left join `drink` on((`restaurant`.`restaurant_id` = `drink`.`restaurant_id`))) left join `drink_has_ingredient` on((`drink`.`drink_id` = `drink_has_ingredient`.`drink_id`))) left join `ingredient` on((`drink_has_ingredient`.`ingredient_name` = `ingredient`.`ingredient_name`))) where (`drink`.`drink_is_approved` = FALSE) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `users_view`
--

/*!50001 DROP VIEW IF EXISTS `users_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`flavoursUser`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `users_view` AS select `restaurant`.`restaurant_id` AS `restaurant_id`,`restaurant`.`restaurant_name` AS `restaurant_name`,`restaurant`.`restaurant_category` AS `restaurant_category`,`restaurant`.`restaurant_longitude` AS `restaurant_longitude`,`restaurant`.`restaurant_latitude` AS `restaurant_latitude`,`restaurant`.`restaurant_opening` AS `restaurant_opening`,`restaurant`.`restaurant_closing` AS `restaurant_closing`,`restaurant`.`user_id` AS `user_id`,NULL AS `food_id`,NULL AS `food_name`,NULL AS `food_description`,NULL AS `food_calories`,NULL AS `drink_id`,NULL AS `drink_name`,NULL AS `drink_description`,NULL AS `ingredient_name`,NULL AS `ingredient_has_alcohol` from `restaurant` where (`restaurant`.`restaurant_is_approved` = TRUE) union select `restaurant`.`restaurant_id` AS `restaurant_id`,`restaurant`.`restaurant_name` AS `restaurant_name`,`restaurant`.`restaurant_category` AS `restaurant_category`,`restaurant`.`restaurant_longitude` AS `restaurant_longitude`,`restaurant`.`restaurant_latitude` AS `restaurant_latitude`,`restaurant`.`restaurant_opening` AS `restaurant_opening`,`restaurant`.`restaurant_closing` AS `restaurant_closing`,`restaurant`.`user_id` AS `user_id`,`food`.`food_id` AS `food_id`,`food`.`food_name` AS `food_name`,`food`.`food_description` AS `food_description`,`food`.`food_calories` AS `food_calories`,NULL AS `drink_id`,NULL AS `drink_name`,NULL AS `drink_description`,`ingredient`.`ingredient_name` AS `ingredient_name`,`ingredient`.`ingredient_has_alcohol` AS `ingredient_has_alcohol` from (((`restaurant` left join `food` on((`restaurant`.`restaurant_id` = `food`.`restaurant_id`))) left join `food_has_ingredient` on((`food`.`food_id` = `food_has_ingredient`.`food_id`))) left join `ingredient` on((`food_has_ingredient`.`ingredient_name` = `ingredient`.`ingredient_name`))) where ((`restaurant`.`restaurant_is_approved` = TRUE) and (`food`.`food_is_approved` = TRUE)) union select `restaurant`.`restaurant_id` AS `restaurant_id`,`restaurant`.`restaurant_name` AS `restaurant_name`,`restaurant`.`restaurant_category` AS `restaurant_category`,`restaurant`.`restaurant_longitude` AS `restaurant_longitude`,`restaurant`.`restaurant_latitude` AS `restaurant_latitude`,`restaurant`.`restaurant_opening` AS `restaurant_opening`,`restaurant`.`restaurant_closing` AS `restaurant_closing`,`restaurant`.`user_id` AS `user_id`,NULL AS `food_id`,NULL AS `food_name`,NULL AS `food_description`,NULL AS `food_calories`,`drink`.`drink_id` AS `drink_id`,`drink`.`drink_name` AS `drink_name`,`drink`.`drink_description` AS `drink_description`,`ingredient`.`ingredient_name` AS `ingredient_name`,`ingredient`.`ingredient_has_alcohol` AS `ingredient_has_alcohol` from (((`restaurant` left join `drink` on((`restaurant`.`restaurant_id` = `drink`.`restaurant_id`))) left join `drink_has_ingredient` on((`drink`.`drink_id` = `drink_has_ingredient`.`drink_id`))) left join `ingredient` on((`drink_has_ingredient`.`ingredient_name` = `ingredient`.`ingredient_name`))) where ((`restaurant`.`restaurant_is_approved` = TRUE) and (`drink`.`drink_is_approved` = TRUE)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-23 18:52:52
