CREATE DATABASE  IF NOT EXISTS `flavours_without_borders` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `flavours_without_borders`;
-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 83.212.109.171    Database: flavours_without_borders
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `food`
--

DROP TABLE IF EXISTS `food`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food` (
  `food_id` int(11) NOT NULL,
  `food_name` varchar(500) NOT NULL,
  `food_description` varchar(700) DEFAULT NULL,
  `food_calories` int(11) DEFAULT NULL,
  `food_is_approved` bit(1) DEFAULT b'0',
  `restaurant_id` int(11) NOT NULL,
  PRIMARY KEY (`food_id`),
  UNIQUE KEY `food_id_UNIQUE` (`food_id`),
  KEY `food_is_offered_by_restaurant_id_idx` (`restaurant_id`),
  CONSTRAINT `food_is_offered_by_restaurant_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`restaurant_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food`
--

LOCK TABLES `food` WRITE;
/*!40000 ALTER TABLE `food` DISABLE KEYS */;
INSERT INTO `food` VALUES (2,'Κεφτεδάκια με πατάτες',NULL,450,_binary '',8132),(4,'Κοτομπουκιές',NULL,NULL,_binary '\0',4330),(6,'Γεμιστά','Λαχταριστές ντομάτες και πιπεριές γεμιστές',326,_binary '',8),(8,'Κεφτεδάκια με πατάτες',NULL,NULL,_binary '\0',8),(48,'Burger','Είναι σαν να τρως αέρα!',1821,_binary '',15),(68,'Μπριζολάκια','Χοιρινές μπριζόλες ξεροψημένες!',NULL,_binary '\0',8132),(73,'Τυροκαυτερή','Η καλύτερη συνοδεία για τα τηγανητά κολοκυθάκια',276,_binary '',8132),(124,'Γεμιστά','Παραδοσιακό ελληνικό πιάτο',392,_binary '',4330),(478,'Πατσάς με σκορδοστούμπι','Ένα πιάτο που θυμίζει πολύ χωριό',450,_binary '',4330),(548,'Fish&Chips','Μία γεύση από Αγγλία',840,_binary '',7365);
/*!40000 ALTER TABLE `food` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-23 18:53:50
