CREATE DATABASE  IF NOT EXISTS `flavours_without_borders` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `flavours_without_borders`;
-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 83.212.109.171    Database: flavours_without_borders
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `drink_has_ingredient`
--

DROP TABLE IF EXISTS `drink_has_ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drink_has_ingredient` (
  `drink_id` int(11) NOT NULL,
  `ingredient_name` varchar(500) NOT NULL,
  PRIMARY KEY (`drink_id`,`ingredient_name`),
  KEY `ingredient_name_idx` (`ingredient_name`),
  CONSTRAINT `drink_has_drink_id` FOREIGN KEY (`drink_id`) REFERENCES `drink` (`drink_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `drink_has_ingredient_name` FOREIGN KEY (`ingredient_name`) REFERENCES `ingredient` (`ingredient_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drink_has_ingredient`
--

LOCK TABLES `drink_has_ingredient` WRITE;
/*!40000 ALTER TABLE `drink_has_ingredient` DISABLE KEYS */;
INSERT INTO `drink_has_ingredient` VALUES (26427,'Angostura bitters'),(3214,'club soda'),(6363,'coffee liqueur'),(46,'Cola'),(984,'Cola'),(6363,'cream'),(46,'Gin'),(984,'Gin'),(984,'Lemon juice'),(46,'Lemon slice'),(3214,'lime'),(3214,'mint'),(3214,'rum'),(46,'Silver tequila'),(984,'Silver tequila'),(3214,'sugar'),(26427,'sugar'),(46,'Sweet and sour mix'),(46,'Triple sec'),(984,'Triple sec'),(6363,'vodka'),(26427,'whiskey'),(46,'White rum'),(984,'White rum');
/*!40000 ALTER TABLE `drink_has_ingredient` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-23 18:53:49
