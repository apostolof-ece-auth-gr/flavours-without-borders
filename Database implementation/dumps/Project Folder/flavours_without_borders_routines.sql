CREATE DATABASE  IF NOT EXISTS `flavours_without_borders` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `flavours_without_borders`;
-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 83.212.109.171    Database: flavours_without_borders
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `mods_view`
--

DROP TABLE IF EXISTS `mods_view`;
/*!50001 DROP VIEW IF EXISTS `mods_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `mods_view` AS SELECT 
 1 AS `restaurant_id`,
 1 AS `restaurant_name`,
 1 AS `restaurant_category`,
 1 AS `restaurant_longitude`,
 1 AS `restaurant_latitude`,
 1 AS `restaurant_opening`,
 1 AS `restaurant_closing`,
 1 AS `user_id`,
 1 AS `food_id`,
 1 AS `food_name`,
 1 AS `food_description`,
 1 AS `food_calories`,
 1 AS `drink_id`,
 1 AS `drink_name`,
 1 AS `drink_description`,
 1 AS `ingredient_name`,
 1 AS `ingredient_has_alcohol`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `users_view`
--

DROP TABLE IF EXISTS `users_view`;
/*!50001 DROP VIEW IF EXISTS `users_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `users_view` AS SELECT 
 1 AS `restaurant_id`,
 1 AS `restaurant_name`,
 1 AS `restaurant_category`,
 1 AS `restaurant_longitude`,
 1 AS `restaurant_latitude`,
 1 AS `restaurant_opening`,
 1 AS `restaurant_closing`,
 1 AS `user_id`,
 1 AS `food_id`,
 1 AS `food_name`,
 1 AS `food_description`,
 1 AS `food_calories`,
 1 AS `drink_id`,
 1 AS `drink_name`,
 1 AS `drink_description`,
 1 AS `ingredient_name`,
 1 AS `ingredient_has_alcohol`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `mods_view`
--

/*!50001 DROP VIEW IF EXISTS `mods_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`flavoursUser`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `mods_view` AS select `restaurant`.`restaurant_id` AS `restaurant_id`,`restaurant`.`restaurant_name` AS `restaurant_name`,`restaurant`.`restaurant_category` AS `restaurant_category`,`restaurant`.`restaurant_longitude` AS `restaurant_longitude`,`restaurant`.`restaurant_latitude` AS `restaurant_latitude`,`restaurant`.`restaurant_opening` AS `restaurant_opening`,`restaurant`.`restaurant_closing` AS `restaurant_closing`,`restaurant`.`user_id` AS `user_id`,NULL AS `food_id`,NULL AS `food_name`,NULL AS `food_description`,NULL AS `food_calories`,NULL AS `drink_id`,NULL AS `drink_name`,NULL AS `drink_description`,NULL AS `ingredient_name`,NULL AS `ingredient_has_alcohol` from `restaurant` where (`restaurant`.`restaurant_is_approved` = FALSE) union select `restaurant`.`restaurant_id` AS `restaurant_id`,`restaurant`.`restaurant_name` AS `restaurant_name`,`restaurant`.`restaurant_category` AS `restaurant_category`,`restaurant`.`restaurant_longitude` AS `restaurant_longitude`,`restaurant`.`restaurant_latitude` AS `restaurant_latitude`,`restaurant`.`restaurant_opening` AS `restaurant_opening`,`restaurant`.`restaurant_closing` AS `restaurant_closing`,`restaurant`.`user_id` AS `user_id`,`food`.`food_id` AS `food_id`,`food`.`food_name` AS `food_name`,`food`.`food_description` AS `food_description`,`food`.`food_calories` AS `food_calories`,NULL AS `drink_id`,NULL AS `drink_name`,NULL AS `drink_description`,`ingredient`.`ingredient_name` AS `ingredient_name`,`ingredient`.`ingredient_has_alcohol` AS `ingredient_has_alcohol` from (((`restaurant` left join `food` on((`restaurant`.`restaurant_id` = `food`.`restaurant_id`))) left join `food_has_ingredient` on((`food`.`food_id` = `food_has_ingredient`.`food_id`))) left join `ingredient` on((`food_has_ingredient`.`ingredient_name` = `ingredient`.`ingredient_name`))) where (`food`.`food_is_approved` = FALSE) union select `restaurant`.`restaurant_id` AS `restaurant_id`,`restaurant`.`restaurant_name` AS `restaurant_name`,`restaurant`.`restaurant_category` AS `restaurant_category`,`restaurant`.`restaurant_longitude` AS `restaurant_longitude`,`restaurant`.`restaurant_latitude` AS `restaurant_latitude`,`restaurant`.`restaurant_opening` AS `restaurant_opening`,`restaurant`.`restaurant_closing` AS `restaurant_closing`,`restaurant`.`user_id` AS `user_id`,NULL AS `food_id`,NULL AS `food_name`,NULL AS `food_description`,NULL AS `food_calories`,`drink`.`drink_id` AS `drink_id`,`drink`.`drink_name` AS `drink_name`,`drink`.`drink_description` AS `drink_description`,`ingredient`.`ingredient_name` AS `ingredient_name`,`ingredient`.`ingredient_has_alcohol` AS `ingredient_has_alcohol` from (((`restaurant` left join `drink` on((`restaurant`.`restaurant_id` = `drink`.`restaurant_id`))) left join `drink_has_ingredient` on((`drink`.`drink_id` = `drink_has_ingredient`.`drink_id`))) left join `ingredient` on((`drink_has_ingredient`.`ingredient_name` = `ingredient`.`ingredient_name`))) where (`drink`.`drink_is_approved` = FALSE) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `users_view`
--

/*!50001 DROP VIEW IF EXISTS `users_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`flavoursUser`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `users_view` AS select `restaurant`.`restaurant_id` AS `restaurant_id`,`restaurant`.`restaurant_name` AS `restaurant_name`,`restaurant`.`restaurant_category` AS `restaurant_category`,`restaurant`.`restaurant_longitude` AS `restaurant_longitude`,`restaurant`.`restaurant_latitude` AS `restaurant_latitude`,`restaurant`.`restaurant_opening` AS `restaurant_opening`,`restaurant`.`restaurant_closing` AS `restaurant_closing`,`restaurant`.`user_id` AS `user_id`,NULL AS `food_id`,NULL AS `food_name`,NULL AS `food_description`,NULL AS `food_calories`,NULL AS `drink_id`,NULL AS `drink_name`,NULL AS `drink_description`,NULL AS `ingredient_name`,NULL AS `ingredient_has_alcohol` from `restaurant` where (`restaurant`.`restaurant_is_approved` = TRUE) union select `restaurant`.`restaurant_id` AS `restaurant_id`,`restaurant`.`restaurant_name` AS `restaurant_name`,`restaurant`.`restaurant_category` AS `restaurant_category`,`restaurant`.`restaurant_longitude` AS `restaurant_longitude`,`restaurant`.`restaurant_latitude` AS `restaurant_latitude`,`restaurant`.`restaurant_opening` AS `restaurant_opening`,`restaurant`.`restaurant_closing` AS `restaurant_closing`,`restaurant`.`user_id` AS `user_id`,`food`.`food_id` AS `food_id`,`food`.`food_name` AS `food_name`,`food`.`food_description` AS `food_description`,`food`.`food_calories` AS `food_calories`,NULL AS `drink_id`,NULL AS `drink_name`,NULL AS `drink_description`,`ingredient`.`ingredient_name` AS `ingredient_name`,`ingredient`.`ingredient_has_alcohol` AS `ingredient_has_alcohol` from (((`restaurant` left join `food` on((`restaurant`.`restaurant_id` = `food`.`restaurant_id`))) left join `food_has_ingredient` on((`food`.`food_id` = `food_has_ingredient`.`food_id`))) left join `ingredient` on((`food_has_ingredient`.`ingredient_name` = `ingredient`.`ingredient_name`))) where ((`restaurant`.`restaurant_is_approved` = TRUE) and (`food`.`food_is_approved` = TRUE)) union select `restaurant`.`restaurant_id` AS `restaurant_id`,`restaurant`.`restaurant_name` AS `restaurant_name`,`restaurant`.`restaurant_category` AS `restaurant_category`,`restaurant`.`restaurant_longitude` AS `restaurant_longitude`,`restaurant`.`restaurant_latitude` AS `restaurant_latitude`,`restaurant`.`restaurant_opening` AS `restaurant_opening`,`restaurant`.`restaurant_closing` AS `restaurant_closing`,`restaurant`.`user_id` AS `user_id`,NULL AS `food_id`,NULL AS `food_name`,NULL AS `food_description`,NULL AS `food_calories`,`drink`.`drink_id` AS `drink_id`,`drink`.`drink_name` AS `drink_name`,`drink`.`drink_description` AS `drink_description`,`ingredient`.`ingredient_name` AS `ingredient_name`,`ingredient`.`ingredient_has_alcohol` AS `ingredient_has_alcohol` from (((`restaurant` left join `drink` on((`restaurant`.`restaurant_id` = `drink`.`restaurant_id`))) left join `drink_has_ingredient` on((`drink`.`drink_id` = `drink_has_ingredient`.`drink_id`))) left join `ingredient` on((`drink_has_ingredient`.`ingredient_name` = `ingredient`.`ingredient_name`))) where ((`restaurant`.`restaurant_is_approved` = TRUE) and (`drink`.`drink_is_approved` = TRUE)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-23 18:54:03
