CREATE DATABASE  IF NOT EXISTS `flavours_without_borders` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `flavours_without_borders`;
-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 83.212.109.171    Database: flavours_without_borders
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_rates_drink`
--

DROP TABLE IF EXISTS `user_rates_drink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_rates_drink` (
  `user_id` int(11) NOT NULL,
  `drink_id` int(11) NOT NULL,
  `rating_grade` int(11) NOT NULL,
  `rating_date` date NOT NULL,
  `rating_text` varchar(700) DEFAULT NULL,
  `rating_pοrtion_size` enum('small','medium','big') DEFAULT NULL,
  PRIMARY KEY (`user_id`,`drink_id`),
  KEY `user_rates_drink_id_idx` (`drink_id`),
  CONSTRAINT `user_rates_drink_has_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_rates_drink_id` FOREIGN KEY (`drink_id`) REFERENCES `drink` (`drink_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_rates_drink`
--

LOCK TABLES `user_rates_drink` WRITE;
/*!40000 ALTER TABLE `user_rates_drink` DISABLE KEYS */;
INSERT INTO `user_rates_drink` VALUES (7,46,5,'2016-10-12','Πολύ σωστά φτιαγμένο!','big'),(7,984,4,'2017-12-06','Αρκετά καλό','medium'),(9,26427,2,'2018-06-28','Μετριότης κύριοι.','small'),(12,3214,4,'2018-12-17',NULL,'medium'),(12,6363,5,'2018-07-08','Τέλειο!!1',NULL),(215,3214,5,'2018-05-01',NULL,NULL),(666,494,5,'2015-06-11',NULL,NULL),(8756,494,5,'2017-05-10','ΜΠΥΡΑΡΑΑΑΑΑΑΑ!!!!!',NULL),(75813,3,3,'2018-11-04','Δεν ήταν φρέσκια',NULL),(75813,69,4,'2018-04-05',NULL,'medium');
/*!40000 ALTER TABLE `user_rates_drink` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-23 18:53:55
