CREATE DATABASE  IF NOT EXISTS `flavours_without_borders` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `flavours_without_borders`;
-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 83.212.109.171    Database: flavours_without_borders
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `food_has_ingredient`
--

DROP TABLE IF EXISTS `food_has_ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_has_ingredient` (
  `food_id` int(11) NOT NULL,
  `ingredient_name` varchar(500) NOT NULL,
  PRIMARY KEY (`food_id`,`ingredient_name`),
  KEY `ingredient_name_idx` (`ingredient_name`),
  CONSTRAINT `food_has_food_id` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `food_has_ingredient_name` FOREIGN KEY (`ingredient_name`) REFERENCES `ingredient` (`ingredient_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_has_ingredient`
--

LOCK TABLES `food_has_ingredient` WRITE;
/*!40000 ALTER TABLE `food_has_ingredient` DISABLE KEYS */;
INSERT INTO `food_has_ingredient` VALUES (2,'αλάτι'),(6,'αλάτι'),(8,'αλάτι'),(124,'αλάτι'),(548,'αλάτι'),(2,'κιμάς'),(8,'κιμάς'),(124,'κιμάς'),(6,'κουκουνάρι'),(2,'κρεμμύδι'),(124,'κρεμμύδι'),(2,'λάδι'),(6,'λάδι'),(8,'λάδι'),(124,'λάδι'),(548,'λάδι'),(8,'μαϊντανός'),(124,'μαϊντανός'),(478,'μοσχαρίσια πόδια'),(478,'μπούκοβο'),(6,'ντομάτα'),(8,'ντομάτα'),(124,'ντομάτα'),(2,'πατάτα'),(8,'πατάτα'),(548,'πατάτα'),(6,'πιπεριά'),(124,'πιπεριά'),(6,'ρύζι'),(124,'ρύζι'),(478,'σκόρδο'),(548,'ψάρι');
/*!40000 ALTER TABLE `food_has_ingredient` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-23 18:54:01
