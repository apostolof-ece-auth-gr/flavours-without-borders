SELECT restaurant.restaurant_id, AVG(user_rates_restaurant.rating_grade) AS 'Average rating'
FROM flavours_without_borders.restaurant
JOIN flavours_without_borders.user_rates_restaurant
	ON restaurant.restaurant_id = user_rates_restaurant.restaurant_id
WHERE restaurant_name = "McDonald's"
GROUP BY restaurant_id