SELECT *
FROM flavours_without_borders.food
WHERE food.food_id NOT IN (
    SELECT food.food_id
    FROM (
        SELECT ingredient.ingredient_name
        FROM flavours_without_borders.user_follows_diet
        JOIN flavours_without_borders.diet
            ON user_follows_diet.diet_id = diet.diet_id
        JOIN flavours_without_borders.diet_prohibits_ingredient
            ON diet.diet_id = diet_prohibits_ingredient.diet_id
        JOIN flavours_without_borders.ingredient
            ON diet_prohibits_ingredient.ingredient_name = ingredient.ingredient_name
        WHERE user_follows_diet.user_id = 12
        UNION
        SELECT ingredient.ingredient_name
        FROM flavours_without_borders.user_prohibits_ingredient
        JOIN flavours_without_borders.ingredient
            ON user_prohibits_ingredient.ingredient_name = ingredient.ingredient_name
        WHERE user_prohibits_ingredient.user_id = 12
    ) AS z
    JOIN flavours_without_borders.food_has_ingredient
        ON z.ingredient_name = food_has_ingredient.ingredient_name
    JOIN flavours_without_borders.food
        ON food_has_ingredient.food_id = food.food_id
)
