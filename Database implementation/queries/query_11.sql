SELECT permission.permission_description
FROM flavours_without_borders.user JOIN flavours_without_borders.role_has_permission
    ON user.role_id = role_has_permission.role_id
JOIN flavours_without_borders.permission
    ON role_has_permission.permission_id = permission.permission_id
WHERE user_id = 12
