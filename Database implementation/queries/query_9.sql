SELECT user_follows_diet.diet_id, AVG(user_rates_restaurant.rating_grade) AS 'Average rating'
FROM flavours_without_borders.user_rates_restaurant
JOIN flavours_without_borders.user_follows_diet
	ON user_rates_restaurant.diet_id = user_follows_diet.diet_id
WHERE user_follows_diet.user_id = 1566 AND restaurant_id = 4330
GROUP BY user_follows_diet.diet_id