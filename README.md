# Flavours Without Borders
> A complete database and API design and implementation along with an Android application development.

Flavours Without Borders is an *experimental* application developed as part of the course "Database Systems" assignment, that took place in the Department of Electrical & Computer Engineering at Aristotle University of Thessaloniki in 2018-2019.

---

## Status

As of the completion of the project, Flavours Without Borders will NOT be maintained, excluding, perhaps, rare document updates and tweaks if it becomes incompatible with newer software. The aim is to ensure its originality to the day it was delivered, despite its multiple flaws. By no means should it ever be considered stable or safe to use, as it contains numerous incomplete parts, critical bugs and security vulnerabilities.

---

## Clone

Clone this repo to your local machine using `https://gitlab.com/Apostolof/flavours-without-borders.git`

---

## Repository structure

- `Database design folder`: contains the latex code used to produce the first deliverable of the assignment, in which the purpose and design of the database are documented.
- `Database implementation folder`: contains the second deliverable of the assignment which is composed of dumps of the implemented MySQL database in two formats, SQL files that create the database users and views, a MySQL workbench model output and various example queries.
- `UI`: contains the files for the third deliverable of the assignment
  - `Database API`: contains the files needed to setup the Django API developed for this deliverable.
  - `AndroidApp`: contains the Android Studio project files needed to build the Android application developed for this deliverable.

---

## Setup

Setting up Flavours Without Borders requires a significant amount of work. Please follow carefully the detailed instructions provided in [SETUP.md](/UI/Database%20API/SETUP.md).

After setting up the API you can use the Android app either by building it or using the release [apk provided](/UI/release.apk) in this repository.

---

## Support

Reach out to us:

- [apostolof's email](mailto:apotwohd@gmail.com "apotwohd@gmail.com")
- [charaldp's email](mailto:charaldp@ece.auth.gr "charaldp@ece.auth.gr")
- [tsakonag's email](mailto:tsakonag@auth.gr "tsakonag@auth.gr")

---

## License

[![Beerware License](https://img.shields.io/badge/license-beerware%20%F0%9F%8D%BA-blue.svg)](https://gitlab.com/Apostolof/flavours-without-borders/blob/master/LICENSE.md)