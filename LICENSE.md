"THE BEER-WARE LICENSE"

Copyright (c) 2018 Apostolof, charaldp, tsakonag

As long as you retain this notice you can do whatever you want with this stuff.
If we meet some day, and you think this stuff is worth it, you can buy us a
beer in return.
